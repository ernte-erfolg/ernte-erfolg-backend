FROM openjdk:11-jre-slim-buster as builder

# Set up start script, health check and expose port
COPY target/ernte-erfolg-backend.jar /
ENTRYPOINT ["java","-jar","ernte-erfolg-backend.jar"]
EXPOSE 8080
