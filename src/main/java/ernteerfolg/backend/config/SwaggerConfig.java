package ernteerfolg.backend.config;

import static com.google.common.collect.Lists.newArrayList;
import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_SWAGGER;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Profile(SPRING_PROFILE_SWAGGER)
public class SwaggerConfig {
   
	// See: http://springfox.github.io/springfox/docs/current/
	
   @Bean
   public Docket actuatorApi() {
       return new Docket(DocumentationType.SWAGGER_2)
    		   .groupName("reactWebApp")
               .select()
               .apis(RequestHandlerSelectors.any())
               .paths(PathSelectors.any())
               .build()
               .securityContexts(newArrayList(actuatorSecurityContext()))
               .securitySchemes(newArrayList(new ApiKey("Bearer", "Authorization", "header")));
   }
   
   private springfox.documentation.spi.service.contexts.SecurityContext actuatorSecurityContext() {
       return springfox.documentation.spi.service.contexts.SecurityContext.builder()
               .securityReferences(newArrayList(new SecurityReference("basicAuth", new AuthorizationScope[0])))
               .forPaths(PathSelectors.ant("/actuator/**"))
               .build();
   }   
}