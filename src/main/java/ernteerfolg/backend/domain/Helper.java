package ernteerfolg.backend.domain;


import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Helper.
 */
@Entity
@Table(name = "helper")
public class Helper implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "experience")
    private Long experience;

    @OneToOne
    @JoinColumn(unique = true)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private UserExtend userExtend;

    @OneToMany(mappedBy = "helper")
    private Set<OfferedWorkDay> offeredWorkDays = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExperience() {
        return experience;
    }

    public Helper experience(Long experience) {
        this.experience = experience;
        return this;
    }

    public void setExperience(Long experience) {
        this.experience = experience;
    }

    public UserExtend getUserExtend() {
        return userExtend;
    }

    public Helper userExtend(UserExtend userExtend) {
        this.userExtend = userExtend;
        return this;
    }

    public void setUserExtend(UserExtend userExtend) {
        this.userExtend = userExtend;
    }

    public Set<OfferedWorkDay> getOfferedWorkDays() {
        return offeredWorkDays;
    }

    public Helper helperToDates(Set<OfferedWorkDay> offeredWorkDays) {
        this.offeredWorkDays = offeredWorkDays;
        return this;
    }

    public Helper addHelperToDate(OfferedWorkDay offeredWorkDay) {
        this.offeredWorkDays.add(offeredWorkDay);
        offeredWorkDay.setHelper(this);
        return this;
    }

    public Helper removeHelperToDate(OfferedWorkDay offeredWorkDay) {
        this.offeredWorkDays.remove(offeredWorkDay);
        offeredWorkDay.setHelper(null);
        return this;
    }

    public void setOfferedWorkDays(Set<OfferedWorkDay> offeredWorkDays) {
        this.offeredWorkDays = offeredWorkDays;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Helper)) {
            return false;
        }
        return id != null && id.equals(((Helper) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Helper{" +
            "id=" + getId() +
            ", experience=" + getExperience() +
            "}";
    }


    public Helper offeredWorkDays(Set<OfferedWorkDay> offeredWorkDays) {
        this.offeredWorkDays = offeredWorkDays;
        return this;
    }
}
