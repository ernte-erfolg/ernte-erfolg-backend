package ernteerfolg.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Date.
 */
@Entity
@Table(name = "work_day")
public class Workday implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "duration_per_day")
    private Float durationPerDay;

    @ManyToMany(mappedBy = "helpers")
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();

    @OneToMany(mappedBy = "workday", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<OfferedWorkDay> offeredWorkDays = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDurationPerDay() {
        return durationPerDay;
    }

    public Workday durationPerDay(Float durationPerDay) {
        this.durationPerDay = durationPerDay;
        return this;
    }

    public void setDurationPerDay(Float durationPerDay) {
        this.durationPerDay = durationPerDay;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public Workday tasks(Set<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    public Workday addTask(Task task) {
        this.tasks.add(task);
        task.getHelpers().add(this);
        return this;
    }

    public Workday removeTask(Task task) {
        this.tasks.remove(task);
        task.getHelpers().remove(this);
        return this;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Set<OfferedWorkDay> getOfferedWorkDays() {
        return offeredWorkDays;
    }

    public Workday helperToDates(Set<OfferedWorkDay> offeredWorkDays) {
        this.offeredWorkDays = offeredWorkDays;
        return this;
    }

    public void setOfferedWorkDays(Set<OfferedWorkDay> offeredWorkDays) {
        this.offeredWorkDays = offeredWorkDays;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workday)) {
            return false;
        }
        return id != null && id.equals(((Workday) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Date{" +
            "id=" + getId() +
            "date" + getDate() +
            ", durationPerDay=" + getDurationPerDay() +
            "}";
    }


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Workday id(Long id) {
        this.id = id;
        return this;
    }

    public Workday date(LocalDate date) {
        this.date = date;
        return this;
    }

    public Workday offeredWorkDays(Set<OfferedWorkDay> offeredWorkDays) {
        this.offeredWorkDays = offeredWorkDays;
        return this;
    }
}
