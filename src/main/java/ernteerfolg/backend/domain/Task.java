package ernteerfolg.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Task.
 */
@Entity
@Table(name = "task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "phone_nr")
    private String phoneNr;

    @Column(name = "e_mail")
    private String eMail;

    @Column(name = "number_of_helpers")
    private Long numberOfHelpers;

    @Column(name = "description_place")
    private String descriptionPlace;

    @Column(name = "lon", precision = 21, scale = 2)
    private BigDecimal lon;

    @Column(name = "lat", precision = 21, scale = 2)
    private BigDecimal lat;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "archived")
    private Boolean archived;

    @OneToOne
    @JoinColumn(unique = true)
    private Address address;

    @ManyToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "task_helper",
               joinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "helper_id", referencedColumnName = "id") )
    private Set<Workday> helpers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tasks")
    private Farm farm;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Task title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Task description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public Task phoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
        return this;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String geteMail() {
        return eMail;
    }

    public Task eMail(String eMail) {
        this.eMail = eMail;
        return this;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public Long getNumberOfHelpers() {
        return numberOfHelpers;
    }

    public Task numberOfHelpers(Long numberOfHelpers) {
        this.numberOfHelpers = numberOfHelpers;
        return this;
    }

    public void setNumberOfHelpers(Long numberOfHelpers) {
        this.numberOfHelpers = numberOfHelpers;
    }

    public String getDescriptionPlace() {
        return descriptionPlace;
    }

    public Task descriptionPlace(String descriptionPlace) {
        this.descriptionPlace = descriptionPlace;
        return this;
    }

    public void setDescriptionPlace(String descriptionPlace) {
        this.descriptionPlace = descriptionPlace;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public Task lon(BigDecimal lon) {
        this.lon = lon;
        return this;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public Task lat(BigDecimal lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public Boolean isActive() {
        return active;
    }

    public Task active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean isArchived() {
        return archived;
    }

    public Task archived(Boolean archived) {
        this.archived = archived;
        return this;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Address getAddress() {
        return address;
    }

    public Task address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Workday> getHelpers() {
        return helpers;
    }

    public Task helpers(Set<Workday> workdays) {
        this.helpers = workdays;
        return this;
    }

    public Task addHelper(Workday workday) {
        this.helpers.add(workday);
        workday.getTasks().add(this);
        return this;
    }

    public Task removeHelper(Workday workday) {
        this.helpers.remove(workday);
        workday.getTasks().remove(this);
        return this;
    }

    public void setHelpers(Set<Workday> workdays) {
        this.helpers = workdays;
    }

    public Farm getFarm() {
        return farm;
    }

    public Task farm(Farm farm) {
        this.farm = farm;
        return this;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Task)) {
            return false;
        }
        return id != null && id.equals(((Task) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", phoneNr='" + getPhoneNr() + "'" +
            ", eMail='" + geteMail() + "'" +
            ", numberOfHelpers=" + getNumberOfHelpers() +
            ", descriptionPlace='" + getDescriptionPlace() + "'" +
            ", lon=" + getLon() +
            ", lat=" + getLat() +
            ", active='" + isActive() + "'" +
            ", archived='" + isArchived() + "'" +
            "}";
    }
}
