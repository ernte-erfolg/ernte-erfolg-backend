package ernteerfolg.backend.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A FarmType.
 */
@Entity
@Table(name = "farm_type")
public class FarmType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "farmType")
    private Set<Farm> farms = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public FarmType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Farm> getFarms() {
        return farms;
    }

    public FarmType farms(Set<Farm> farms) {
        this.farms = farms;
        return this;
    }

    public FarmType addFarm(Farm farm) {
        this.farms.add(farm);
        farm.setFarmType(this);
        return this;
    }

    public FarmType removeFarm(Farm farm) {
        this.farms.remove(farm);
        farm.setFarmType(null);
        return this;
    }

    public void setFarms(Set<Farm> farms) {
        this.farms = farms;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FarmType)) {
            return false;
        }
        return id != null && id.equals(((FarmType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FarmType{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
