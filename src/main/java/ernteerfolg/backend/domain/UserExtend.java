package ernteerfolg.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import ernteerfolg.backend.domain.enumeration.UserClass;

/**
 * A UserExtend.
 */
@Entity
@Table(name = "user_extend")
public class UserExtend implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_class")
    private UserClass userClass;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column(name = "phone")
    private String phone;

    @OneToOne(mappedBy = "userExtend")
    @JsonIgnore
    private Farm farm;

    @OneToOne(mappedBy = "userExtend")
    @JsonIgnore
    private Helper helper;

    @ManyToOne
    @JsonIgnoreProperties("userExtends")
    private Address address;

    @OneToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserClass getUserClass() {
        return userClass;
    }

    public UserExtend userClass(UserClass userClass) {
        this.userClass = userClass;
        return this;
    }

    public void setUserClass(UserClass userClass) {
        this.userClass = userClass;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public UserExtend mobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
        return this;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPhone() {
        return phone;
    }

    public UserExtend phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Farm getFarm() {
        return farm;
    }

    public UserExtend farm(Farm farm) {
        this.farm = farm;
        return this;
    }

    public void setFarmer(Farm farm) {
        this.farm = farm;
    }

    public Helper getHelper() {
        return helper;
    }

    public UserExtend helper(Helper helper) {
        this.helper = helper;
        return this;
    }

    public void setHelper(Helper helper) {
        this.helper = helper;
    }

    public Address getAddress() {
        return address;
    }

    public UserExtend address(Address address) {
        this.address = address;
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserExtend)) {
            return false;
        }
        return id != null && id.equals(((UserExtend) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserExtend{" +
            "id=" + getId() +
            ", userClass='" + getUserClass() + "'" +
            ", mobilePhone='" + getMobilePhone() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }


    public UserExtend user(User user) {
        this.user = user;
        return this;
    }
}
