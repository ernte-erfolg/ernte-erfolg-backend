package ernteerfolg.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A HelperToDate.
 */
@Entity
@Table(name = "offered_work_day")
public class OfferedWorkDay implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "success")
    private Boolean success;

    @Column(name = "duration")
    private Float duration;

    @ManyToOne (fetch = FetchType.EAGER)
    private Workday workday;

    @ManyToOne
    private Helper helper;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isSuccess() {
        return success;
    }

    public OfferedWorkDay success(Boolean success) {
        this.success = success;
        return this;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Float getDuration() {
        return duration;
    }

    public OfferedWorkDay duration(Float duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public Workday getWorkdays() {
        return workday;
    }

    public void setWorkdays(Workday workdays) {
        this.workday = workdays;
    }

    public Helper getHelper() {
        return helper;
    }

    public OfferedWorkDay helper(Helper helper) {
        this.helper = helper;
        return this;
    }

    public void setHelper(Helper helper) {
        this.helper = helper;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OfferedWorkDay)) {
            return false;
        }
        return id != null && id.equals(((OfferedWorkDay) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HelperToDate{" +
            "id=" + getId() +
            ", success='" + isSuccess() + "'" +
            ", duration=" + getDuration() +
            "}";
    }

    public OfferedWorkDay id(Long id) {
        this.id = id;
        return this;
    }

    public OfferedWorkDay workday(Workday workday) {
        this.workday = workday;
        return this;
    }
}
