package ernteerfolg.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Farm.
 */
@Entity
@Table(name = "farm")
public class Farm implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "picture")
    private String picture;

    @Column(name = "phone")
    private String phone;

    @Column(name = "e_mail")
    private String eMailFarm;

    @OneToOne
    @JoinColumn(unique = true)
    private Address address;

    @OneToMany(mappedBy = "farm")
    private Set<Task> tasks = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("farms")
    private FarmType farmType;

    @OneToOne
    @JoinColumn(unique = true)
    private UserExtend userExtend;

    @Column(name = "kvm_id")
    private String kvmId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Farm name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Farm description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public String getPicture() {
        return picture;
    }

    public Farm picture(String picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPhone() {
        return phone;
    }

    public Farm phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public Farm address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public Farm tasks(Set<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    public Farm addTask(Task task) {
        this.tasks.add(task);
        task.setFarm(this);
        return this;
    }

    public Farm removeTask(Task task) {
        this.tasks.remove(task);
        task.setFarm(null);
        return this;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public FarmType getFarmType() {
        return farmType;
    }

    public Farm farmType(FarmType farmType) {
        this.farmType = farmType;
        return this;
    }

    public void setFarmType(FarmType farmType) {
        this.farmType = farmType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Farm)) {
            return false;
        }
        return id != null && id.equals(((Farm) o).id);
    }




    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Farm{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", picture='" + getPicture() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }


    public Farm kvmId(String kvmId) {
        this.kvmId = kvmId;
        return this;
    }

    public String getKvmId() {
        return kvmId;
    }

    public void setKvmId(String kvmId) {
        this.kvmId = kvmId;
    }

    public UserExtend getUserExtend() {
        return userExtend;
    }

    public void setUserExtend(UserExtend userExtend) {
        this.userExtend = userExtend;
    }

    public Farm userExtend(UserExtend userExtend) {
        this.userExtend = userExtend;
        return this;
    }

    public String geteMailFarm() {
        return eMailFarm;
    }

    public void seteMailFarm(String eMail) {
        this.eMailFarm = eMail;
    }

    public Farm eMailFarm(String eMail) {
        this.eMailFarm = eMail;
        return this;
    }
}
