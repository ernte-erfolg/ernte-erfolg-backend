package ernteerfolg.backend.domain.enumeration;

/**
 * The UserClass enumeration.
 */
public enum UserClass {
    FARM, HELPER
}
