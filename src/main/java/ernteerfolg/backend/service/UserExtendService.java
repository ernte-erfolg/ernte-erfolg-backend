package ernteerfolg.backend.service;

import ernteerfolg.backend.domain.User;
import ernteerfolg.backend.domain.UserExtend;
import ernteerfolg.backend.service.dto.FarmDTO;
import ernteerfolg.backend.service.dto.UserExtendDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ernteerfolg.backend.domain.UserExtend}.
 */
public interface UserExtendService {

    /**
     * Save a userExtend.
     *
     * @param userExtendDTO the entity to save.
     * @return the persisted entity.
     */
    UserExtendDTO save(UserExtendDTO userExtendDTO);

    UserExtend saveNotDto(UserExtendDTO userExtendDTO);

    UserExtend saveFarm(FarmDTO farmDto);

    /**
     * Get all the userExtends.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserExtendDTO> findAll(Pageable pageable);
    /**
     * Get all the UserExtendDTO where Farmer is {@code null}.
     *
     * @return the list of entities.
     */
    List<UserExtendDTO> findAllWhereFarmIsNull();
    /**
     * Get all the UserExtendDTO where Helper is {@code null}.
     *
     * @return the list of entities.
     */
    List<UserExtendDTO> findAllWhereHelperIsNull();

    /**
     * Get the "id" userExtend.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserExtendDTO> findOne(Long id);

    /**
     * Delete the "id" userExtend.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<UserExtendDTO> findOneExtendUserByUser(User user);
}
