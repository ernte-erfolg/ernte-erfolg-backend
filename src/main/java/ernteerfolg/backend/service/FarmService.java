package ernteerfolg.backend.service;

import ernteerfolg.backend.domain.UserExtend;
import ernteerfolg.backend.service.dto.FarmDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ernteerfolg.backend.domain.Farm}.
 */
public interface FarmService {

    /**
     * Save a farm.
     *
     * @param farmDTO the entity to save.
     * @return the persisted entity.
     */
    FarmDTO save(FarmDTO farmDTO);

    /**
     * Get all the farms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FarmDTO> findAll(Pageable pageable);

    /**
     * Get the "id" farm.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FarmDTO> findOne(Long id);

    /**
     * Delete the "id" farm.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
