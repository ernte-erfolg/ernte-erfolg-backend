package ernteerfolg.backend.service.mapper;


import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.service.dto.FarmDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Farm} and its DTO {@link FarmDTO}.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class, FarmTypeMapper.class, UserExtendMapper.class})
public interface FarmMapper extends EntityMapper<FarmDTO, Farm> {

    @Mapping(source = "eMailFarm", target = "contactMail")
    @Mapping(source = "userExtend.user.email", target = "eMail")
    @Mapping(source = "userExtend.address.lat", target ="lat")
    @Mapping(source = "userExtend.address.lon", target ="lon")
    @Mapping(source = "userExtend.address.street", target ="street")
    @Mapping(source = "userExtend.address.houseNr", target ="houseNr")
    @Mapping(source = "userExtend.address.zip", target ="zipCode")
    @Mapping(source = "userExtend.address.city", target ="city")
    FarmDTO toDto(Farm farm);

    @Mapping(target = "tasks", ignore = true)
    @Mapping(target = "removeTask", ignore = true)
    @Mapping(source = "contactMail", target = "eMailFarm")
    Farm toEntity(FarmDTO farmDTO);

    default Farm fromId(Long id) {
        if (id == null) {
            return null;
        }
        Farm farm = new Farm();
        farm.setId(id);
        return farm;
    }
}
