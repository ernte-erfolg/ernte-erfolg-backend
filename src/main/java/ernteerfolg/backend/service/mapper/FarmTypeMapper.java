package ernteerfolg.backend.service.mapper;


import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.service.dto.FarmTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FarmType} and its DTO {@link FarmTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FarmTypeMapper extends EntityMapper<FarmTypeDTO, FarmType> {


    @Mapping(target = "farms", ignore = true)
    @Mapping(target = "removeFarm", ignore = true)
    FarmType toEntity(FarmTypeDTO farmTypeDTO);

    default FarmType fromId(Long id) {
        if (id == null) {
            return null;
        }
        FarmType farmType = new FarmType();
        farmType.setId(id);
        return farmType;
    }
}
