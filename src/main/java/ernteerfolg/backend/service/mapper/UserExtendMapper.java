package ernteerfolg.backend.service.mapper;


import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.service.dto.UserExtendDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserExtend} and its DTO {@link UserExtendDTO}.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class, HelperMapper.class, UserMapper.class})
public interface UserExtendMapper extends EntityMapper<UserExtendDTO, UserExtend> {

    @Mapping(source = "address.id", target = "addressId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "farm.id", target = "farmId")
    @Mapping(source = "helper.id", target = "helperId")
    UserExtendDTO toDto(UserExtend userExtend);

    @Mapping(target = "farm", ignore = false)
    @Mapping(source = "helperId", target = "helper")
    @Mapping(source = "addressId", target = "address")
    @Mapping(source = "userId", target = "user")
    UserExtend toEntity(UserExtendDTO userExtendDTO);

    default UserExtend fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtend userExtend = new UserExtend();
        userExtend.setId(id);
        return userExtend;
    }
}
