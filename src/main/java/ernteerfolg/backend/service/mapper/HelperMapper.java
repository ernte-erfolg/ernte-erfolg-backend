package ernteerfolg.backend.service.mapper;


import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.repository.HelperRepository;
import ernteerfolg.backend.repository.UserExtendRepository;
import ernteerfolg.backend.repository.UserRepository;
import ernteerfolg.backend.service.dto.HelperDTO;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Mapper for the entity {@link Helper} and its DTO {@link HelperPayload}.
 */

@Service
public class HelperMapper {

    private final UserExtendRepository userExtendRepository;

    private final UserRepository userRepository;

    private final HelperRepository helperRepository;

    public HelperMapper(UserExtendRepository userExtendRepository, UserRepository userRepository, HelperRepository helperRepository) {
        this.userExtendRepository = userExtendRepository;
        this.userRepository = userRepository;
        this.helperRepository = helperRepository;
    }


    public HelperDTO toDto(Helper helper){
        HelperDTO helperDTO = new HelperDTO();
        helperDTO.setId(helper.getId());
        helperDTO.setExperience(helper.getExperience());
        if(helper.getUserExtend() == null){
            helper.setUserExtend(userExtendRepository.findUserExtendByHelperId(helper.getId()));

        }
        helperDTO.setFirstName(helper.getUserExtend().getUser().getFirstName());
        helperDTO.setLastName(helper.getUserExtend().getUser().getLastName());
        helperDTO.setOfferedWorkDay(helper.getOfferedWorkDays());
        helperDTO.
            lat(helper.getUserExtend().getAddress().getLat())
            .lon(helper.getUserExtend().getAddress().getLon())
            .street(helper.getUserExtend().getAddress().getStreet())
            .houseNr(helper.getUserExtend().getAddress().getHouseNr())
            .zipCode(helper.getUserExtend().getAddress().getZip())
            .city(helper.getUserExtend().getAddress().getCity())
            .phone(helper.getUserExtend().getMobilePhone())
            .eMail(helper.getUserExtend().getUser().getEmail());
        return helperDTO;
    }

    public Helper toEntity(HelperDTO helperDTO){
        Helper helper = new Helper();
        helper.setId(helperDTO.getId());
        helper.setExperience(helperDTO.getExperience());
        helper.setOfferedWorkDays(helperDTO.getOfferedWorkDay());

        return helper;
    }

    public Helper fromId(Long id) {
        if (id == null) {
            return null;
        }
        Optional<Helper> helper = helperRepository.findById(id);
        return helper.orElse(null);


    }

}
