package ernteerfolg.backend.service.mapper;


import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.service.dto.WorkdayDTO;

import org.mapstruct.*;

import java.util.Set;

/**
 * Mapper for the entity {@link Workday} and its DTO {@link WorkdayPayload}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WorkdayMapper extends EntityMapper<WorkdayDTO, Workday> {


    @Mapping(target = "tasks", ignore = true)
    @Mapping(target = "removeTask", ignore = true)
    Workday toEntity(WorkdayDTO workdayDTO);

    default Workday fromId(Long id) {
        if (id == null) {
            return null;
        }
        Workday workday = new Workday();
        workday.setId(id);
        return workday;
    }

}
