package ernteerfolg.backend.service.mapper;


import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.service.dto.OfferedWorkDayDTO;

import org.mapstruct.*;


/**
 * Mapper for the entity {@link OfferedWorkDay} and its DTO {@link OfferedWorkDayDTO}.
 */
@Mapper(componentModel = "spring", uses = {WorkdayMapper.class, HelperMapper.class})
public interface OfferedWorkDayMapper extends EntityMapper<OfferedWorkDayDTO, OfferedWorkDay> {

    @Mapping(source = "helper.id", target = "helperId")
     OfferedWorkDayDTO toDto(OfferedWorkDay offeredWorkDay);

    @Mapping(source = "workdayDTO.id", target = "workday.id")
    OfferedWorkDay toEntity(OfferedWorkDayDTO offeredWorkDayDTO);

    default OfferedWorkDay fromId(Long id) {
        if (id == null) {
            return null;
        }
        OfferedWorkDay offeredWorkDay = new OfferedWorkDay();
        offeredWorkDay.setId(id);
        return offeredWorkDay;
    }
}
