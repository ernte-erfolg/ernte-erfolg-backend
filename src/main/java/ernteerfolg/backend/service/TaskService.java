package ernteerfolg.backend.service;

import java.util.List;

import org.opengis.referencing.operation.TransformException;

import ernteerfolg.backend.service.dto.TasksInAreaDTO;

/**
 * Service Interface for managing {@link ernteerfolg.backend.domain.Task}.
 */
public interface TaskService {
		
	List<TasksInAreaDTO> searchTasks(double lat, double lon) throws TransformException;
}
