package ernteerfolg.backend.service;

import ernteerfolg.backend.service.dto.HelperDTO;

import ernteerfolg.backend.service.dto.OfferDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.Set;

/**
 * Service Interface for managing {@link ernteerfolg.backend.domain.Helper}.
 */
public interface HelperService {

    /**
     * Save a helper.
     *
     * @param helperDTO the entity to save.
     * @return the persisted entity.
     */
    HelperDTO save(HelperDTO helperDTO);

    /**
     * Get all the helpers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HelperDTO> findAll(Pageable pageable);

    /**
     * Get the "id" helper.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HelperDTO> findOne(Long id);

    /**
     * Delete the "id" helper.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    OfferDTO getMyOfferForTask(Long farmId);

    OfferDTO setOfferForTask(OfferDTO offerDTO);

    Set<OfferDTO> getMyOffers();
}
