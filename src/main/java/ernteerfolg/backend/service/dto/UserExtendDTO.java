package ernteerfolg.backend.service.dto;

import ernteerfolg.backend.domain.User;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.UserExtend} entity.
 */
public class UserExtendDTO implements Serializable {

    private Long id;

    private String mobilePhone;

    private String phone;

    private Long addressId;

    private Long userId;

    private Long farmId;

    private Long helperId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFarmId() {
        return farmId;
    }

    public void setFarmId(Long farmId) {
        this.farmId = farmId;
    }

    public Long getHelperId() {
        return helperId;
    }

    public void setHelperId(Long helperId) {
        this.helperId = helperId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserExtendDTO userExtendDTO = (UserExtendDTO) o;
        if (userExtendDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtendDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserExtendDTO{" +
            "id=" + getId() +
            ", mobilePhone='" + getMobilePhone() + "'" +
            ", phone='" + getPhone() + "'" +
            ", addressId=" + getAddressId() +
            "}";
    }
}
