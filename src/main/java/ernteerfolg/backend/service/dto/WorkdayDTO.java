package ernteerfolg.backend.service.dto;

import ernteerfolg.backend.domain.Workday;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link Workday} entity.
 */
public class WorkdayDTO implements Serializable {

    private Long id;

    private LocalDate date;

    private Float durationPerDay;

    private Set <OfferedWorkDayDTO> offeredWorkDayDTOS;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getDurationPerDay() {
        return durationPerDay;
    }

    public void setDurationPerDay(Float durationPerDay) {
        this.durationPerDay = durationPerDay;
    }

    public Set<OfferedWorkDayDTO> getOfferedWorkDayDTOS() {
        return offeredWorkDayDTOS;
    }

    public void setOfferedWorkDayDTOS(Set<OfferedWorkDayDTO> offeredWorkDayDTOS) {
        this.offeredWorkDayDTOS = offeredWorkDayDTOS;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkdayDTO workdayDTO = (WorkdayDTO) o;
        if (workdayDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), workdayDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DateDTO{" +
            "id=" + getId() +
            "date" + getDate() +
            ", durationPerDay=" + getDurationPerDay() +
            "}";
    }

    public WorkdayDTO id(Long id) {
        this.id = id;
        return this;
    }

    public WorkdayDTO date(LocalDate date) {
        this.date = date;
        return this;
    }

    public WorkdayDTO durationPerDay(Float durationPerDay) {
        this.durationPerDay = durationPerDay;
        return this;
    }

    public WorkdayDTO offeredWorkDayDTOS(Set<OfferedWorkDayDTO> offeredWorkDayDTOS) {
        this.offeredWorkDayDTOS = offeredWorkDayDTOS;
        return this;
    }
}
