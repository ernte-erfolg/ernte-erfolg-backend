package ernteerfolg.backend.service.dto;

import ernteerfolg.backend.domain.OfferedWorkDay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.Helper} entity.
 */
public class HelperDTO implements Serializable {

    private Long id;

    private Long experience;

    private String eMail;

    private String firstName;

    private String LastName;

    private BigDecimal lat;

    private BigDecimal lon;

    private String street;

    private String houseNr;

    private String zipCode;

    private String city;

    private String phone;

    private Set<OfferedWorkDay> offeredWorkDay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExperience() {
        return experience;
    }

    public void setExperience(Long experience) {
        this.experience = experience;
    }

    public Set<OfferedWorkDay> getOfferedWorkDay() {
        return offeredWorkDay;
    }

    public void setOfferedWorkDay(Set<OfferedWorkDay> offeredWorkDay) {
        this.offeredWorkDay = offeredWorkDay;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HelperDTO helperDTO = (HelperDTO) o;
        if (helperDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), helperDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HelperPayload{" +
            "id=" + getId() +
            ", experience=" + getExperience() +
            "}";
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(String houseNr) {
        this.houseNr = houseNr;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public HelperDTO id(Long id) {
        this.id = id;
        return this;
    }

    public HelperDTO experience(Long experience) {
        this.experience = experience;
        return this;
    }

    public HelperDTO eMail(String eMail) {
        this.eMail = eMail;
        return this;
    }

    public HelperDTO firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public HelperDTO LastName(String LastName) {
        this.LastName = LastName;
        return this;
    }

    public HelperDTO lat(BigDecimal lat) {
        this.lat = lat;
        return this;
    }

    public HelperDTO lon(BigDecimal lon) {
        this.lon = lon;
        return this;
    }

    public HelperDTO street(String street) {
        this.street = street;
        return this;
    }

    public HelperDTO houseNr(String houseNr) {
        this.houseNr = houseNr;
        return this;
    }

    public HelperDTO zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public HelperDTO city(String city) {
        this.city = city;
        return this;
    }

    public HelperDTO phone(String phone) {
        this.phone = phone;
        return this;
    }

    public HelperDTO helperToDate(Set<OfferedWorkDay> offeredWorkDay) {
        this.offeredWorkDay = offeredWorkDay;
        return this;
    }
}
