package ernteerfolg.backend.service.dto;

import ernteerfolg.backend.domain.OfferedWorkDay;
import ernteerfolg.backend.domain.Workday;

import java.util.Set;

public class OfferDTO {

    private Long taskId;

    private Long farmId;

    private String farmName;

    private String title;

    private Set<WorkdayDTO> requestedDays;

    private Set<OfferedWorkDayDTO> guaranteedDays;

    private Set<OfferedWorkDayDTO> myGuaranteedDays;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getFarmId() {
        return farmId;
    }

    public void setFarmId(Long farmId) {
        this.farmId = farmId;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Set<OfferedWorkDayDTO> getGuaranteedDays() {
        return guaranteedDays;
    }

    public void setGuaranteedDays(Set<OfferedWorkDayDTO> guaranteedDays) {
        this.guaranteedDays = guaranteedDays;
    }


    public OfferDTO taskId(Long taskId) {
        this.taskId = taskId;
        return this;
    }

    public OfferDTO farmId(Long farmId) {
        this.farmId = farmId;
        return this;
    }

    public OfferDTO farmName(String farmName) {
        this.farmName = farmName;
        return this;
    }

    public OfferDTO title(String title) {
        this.title = title;
        return this;
    }

    public Set<WorkdayDTO> getRequestedDays() {
        return requestedDays;
    }

    public void setRequestedDays(Set<WorkdayDTO> requestedDays) {
        this.requestedDays = requestedDays;
    }

    public Set<OfferedWorkDayDTO> getMyGuaranteedDays() {
        return myGuaranteedDays;
    }

    public void setMyGuaranteedDays(Set<OfferedWorkDayDTO> myGuaranteedDays) {
        this.myGuaranteedDays = myGuaranteedDays;
    }


    public OfferDTO requestedDays(Set<WorkdayDTO> requestedDays) {
        this.requestedDays = requestedDays;
        return this;
    }

    public OfferDTO guaranteedDays(Set<OfferedWorkDayDTO> guaranteedDays) {
        this.guaranteedDays = guaranteedDays;
        return this;
    }

    public OfferDTO myGuaranteedDays(Set<OfferedWorkDayDTO> myGuaranteedDays) {
        this.myGuaranteedDays = myGuaranteedDays;
        return this;
    }
}
