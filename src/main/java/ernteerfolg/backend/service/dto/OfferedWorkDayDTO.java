package ernteerfolg.backend.service.dto;

import ernteerfolg.backend.domain.OfferedWorkDay;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link OfferedWorkDay} entity.
 */
public class OfferedWorkDayDTO implements Serializable {

    private Long id;

    private Boolean success;

    private Float duration;

    private WorkdayDTO workdayDTO;

    private Long helperId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public WorkdayDTO getWorkdayDTO() {
        return workdayDTO;
    }

    public void setWorkdayDTO(WorkdayDTO workdayDTO) {
        this.workdayDTO = workdayDTO;
    }

    public Long getHelperId() {
        return helperId;
    }

    public void setHelperId(Long helperId) {
        this.helperId = helperId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OfferedWorkDayDTO offeredWorkDayDTO = (OfferedWorkDayDTO) o;
        if (offeredWorkDayDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), offeredWorkDayDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HelperToDateDTO{" +
            "id=" + getId() +
            ", success='" + isSuccess() + "'" +
            ", duration=" + getDuration() +
            ", helperId=" + getHelperId() +
            "}";
    }


    public OfferedWorkDayDTO success(Boolean success) {
        this.success = success;
        return this;
    }

    public OfferedWorkDayDTO duration(Float duration) {
        this.duration = duration;
        return this;
    }

    public OfferedWorkDayDTO workdaysDTO(WorkdayDTO workdaysDTO) {
        this.workdayDTO = workdaysDTO;
        return this;
    }

    public OfferedWorkDayDTO helperId(Long helperId) {
        this.helperId = helperId;
        return this;
    }
}
