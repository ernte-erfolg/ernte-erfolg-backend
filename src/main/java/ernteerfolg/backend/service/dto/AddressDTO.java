package ernteerfolg.backend.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.Address} entity.
 */
public class AddressDTO implements Serializable {

    private Long id;

    private String street;

    private String houseNr;

    private String city;

    private String zip;

    private BigDecimal lon;

    private BigDecimal lat;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(String houseNr) {
        this.houseNr = houseNr;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AddressDTO addressDTO = (AddressDTO) o;
        if (addressDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), addressDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AddressDTO{" +
            "id=" + getId() +
            ", street='" + getStreet() + "'" +
            ", houseNr='" + getHouseNr() + "'" +
            ", city='" + getCity() + "'" +
            ", zip='" + getZip() + "'" +
            ", lon=" + getLon() +
            ", lat=" + getLat() +
            "}";
    }


    public AddressDTO id(Long id) {
        this.id = id;
        return this;
    }

    public AddressDTO street(String street) {
        this.street = street;
        return this;
    }

    public AddressDTO houseNr(String houseNr) {
        this.houseNr = houseNr;
        return this;
    }

    public AddressDTO city(String city) {
        this.city = city;
        return this;
    }

    public AddressDTO zip(String zip) {
        this.zip = zip;
        return this;
    }

    public AddressDTO lon(BigDecimal lon) {
        this.lon = lon;
        return this;
    }

    public AddressDTO lat(BigDecimal lat) {
        this.lat = lat;
        return this;
    }
}
