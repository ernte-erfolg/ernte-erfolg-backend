package ernteerfolg.backend.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.FarmType} entity.
 */
public class FarmTypeDTO implements Serializable {
    
    private Long id;

    private String description;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FarmTypeDTO farmTypeDTO = (FarmTypeDTO) o;
        if (farmTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), farmTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FarmTypeDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
