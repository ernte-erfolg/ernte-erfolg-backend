package ernteerfolg.backend.service.dto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ernteerfolg.backend.domain.Task;

public class TasksInAreaDTO {

	private Set<Task> taskList = new HashSet<>();
	private Map<Long,Double> taskIdDistanceMap = new HashMap<>();
	private int maxDistance;
	
	public TasksInAreaDTO(int maxDistance) {
		super();
		this.maxDistance = maxDistance;
	}

	public Set<Task> getTaskList() {
		return taskList;
	}

	public void add(Task task, Double distance) {
		this.taskList.add(task);
		this.taskIdDistanceMap.put(task.getId(), distance);
	}

	public Map<Long,Double> getTaskIdDistanceMap() {
		return taskIdDistanceMap;
	}

	public int getMaxDistance() {
		return maxDistance;
	}
}
