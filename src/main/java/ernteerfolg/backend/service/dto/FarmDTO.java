package ernteerfolg.backend.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.Farm} entity.
 */
public class FarmDTO implements Serializable {

    private Long id;

    private String name;

    private String contactMail;

    private String description;

    private String picture;

    private String phone;

    private String kvmId;

    private BigDecimal lat;

    private BigDecimal lon;

    private String street;

    private String houseNr;

    private String zipCode;

    private String city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getKvmId() {
        return kvmId;
    }

    public void setKvmId(String kvmId) {
        this.kvmId = kvmId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FarmDTO farmDTO = (FarmDTO) o;
        if (farmDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), farmDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FarmDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", picture='" + getPicture() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }

    public FarmDTO id(Long id) {
        this.id = id;
        return this;
    }

    public FarmDTO name(String name) {
        this.name = name;
        return this;
    }

    public FarmDTO description(String description) {
        this.description = description;
        return this;
    }

    public FarmDTO picture(String picture) {
        this.picture = picture;
        return this;
    }

    public FarmDTO phone(String phone) {
        this.phone = phone;
        return this;
    }

    public FarmDTO kvmId(String kvmId) {
        this.kvmId = kvmId;
        return this;
    }

    public String geteMail() {
        return contactMail;
    }

    public void seteMail(String eMail) {
        this.contactMail = eMail;
    }


    public FarmDTO eMail(String eMail) {
        this.contactMail = eMail;
        return this;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNr;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNr = houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public FarmDTO contacteMail(String contacteMail) {
        this.contactMail = contacteMail;
        return this;
    }

    public FarmDTO lat(BigDecimal lat) {
        this.lat = lat;
        return this;
    }

    public FarmDTO lon(BigDecimal lon) {
        this.lon = lon;
        return this;
    }

    public FarmDTO street(String street) {
        this.street = street;
        return this;
    }

    public FarmDTO houseNr(String houseNr) {
        this.houseNr = houseNr;
        return this;
    }

    public FarmDTO zip(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public FarmDTO city(String city) {
        this.city = city;
        return this;
    }
}
