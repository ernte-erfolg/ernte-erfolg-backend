package ernteerfolg.backend.service.impl;

import ernteerfolg.backend.domain.Address;
import ernteerfolg.backend.domain.User;
import ernteerfolg.backend.repository.AddressRepository;
import ernteerfolg.backend.repository.UserRepository;
import ernteerfolg.backend.security.SecurityUtils;
import ernteerfolg.backend.service.UserExtendService;
import ernteerfolg.backend.domain.UserExtend;
import ernteerfolg.backend.repository.UserExtendRepository;
import ernteerfolg.backend.service.dto.FarmDTO;
import ernteerfolg.backend.service.dto.HelperDTO;
import ernteerfolg.backend.service.dto.UserExtendDTO;
import ernteerfolg.backend.service.mapper.UserExtendMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link UserExtend}.
 */
@Service
@Transactional
public class UserExtendServiceImpl implements UserExtendService {

    private final Logger log = LoggerFactory.getLogger(UserExtendServiceImpl.class);

    private final UserExtendRepository userExtendRepository;

    private final UserExtendMapper userExtendMapper;

    private final UserRepository userRepository;

    private final AddressRepository addressRepository;


    public UserExtendServiceImpl(UserExtendRepository userExtendRepository, UserExtendMapper userExtendMapper, UserRepository userRepository, AddressRepository addressRepository) {
        this.userExtendRepository = userExtendRepository;
        this.userExtendMapper = userExtendMapper;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
    }

    /**
     * Save a userExtend.
     *
     * @param userExtendDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserExtendDTO save(UserExtendDTO userExtendDTO) {
        log.debug("Request to save UserExtend : {}", userExtendDTO);
        UserExtend userExtend = userExtendMapper.toEntity(userExtendDTO);
        User user = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin).get();
        userExtend.setUser(user);
        userExtend = userExtendRepository.saveAndFlush(userExtend);
        return userExtendMapper.toDto(userExtend);
    }

    @Override
    public UserExtend saveNotDto(UserExtendDTO userExtendDTO) {
        log.debug("Request to save UserExtend : {}", userExtendDTO);
        UserExtend userExtend = userExtendMapper.toEntity(userExtendDTO);
        User user = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin).get();
        userExtend.setUser(user);
        userExtend = userExtendRepository.save(userExtend);
        return userExtend;
    }

    public UserExtend saveHelper (HelperDTO helperDTO){
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();
        if(user == null){
            return null;
        }
        user.setFirstName(helperDTO.getFirstName());
        user.setLastName(helperDTO.getLastName());
        user = userRepository.save(user);

        UserExtend userExtend = getUserExtend(user);
        Address address = userExtend.getAddress();
        if(address == null){
            address = new Address();
        }
        address
            .street(helperDTO.getStreet())
            .houseNr(helperDTO.getHouseNr())
            .zip(helperDTO.getZipCode())
            .city(helperDTO.getCity())
            .lat(helperDTO.getLat())
            .lon(helperDTO.getLon());
        address = addressRepository.save(address);
        userExtend.setAddress(address);
        userExtend.setPhone(helperDTO.getPhone());
        return userExtendRepository.save(userExtend);
    }



    public UserExtend saveFarm(FarmDTO farmDto){
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();
        if(user == null){
            return null;
        }
        UserExtend userExtend = getUserExtend(user);
        Address address = userExtend.getAddress();
        if(address == null){
            address = new Address();
        }
        address
            .street(farmDto.getStreet())
            .houseNr(farmDto.getHouseNumber())
            .zip(farmDto.getZipCode())
            .city(farmDto.getCity())
            .lat(farmDto.getLat())
            .lon(farmDto.getLon());
        address = addressRepository.save(address);
        userExtend.setAddress(address);
        userExtend.setPhone(farmDto.getPhone());
        return userExtendRepository.save(userExtend);
    }




    /**
     * Get all the userExtends.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserExtendDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserExtends");
        return userExtendRepository.findAll(pageable)
            .map(userExtendMapper::toDto);
    }


    /**
     *  Get all the userExtends where Farmer is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UserExtendDTO> findAllWhereFarmIsNull() {
        log.debug("Request to get all userExtends where Farmer is null");
        return StreamSupport
            .stream(userExtendRepository.findAll().spliterator(), false)
            .filter(userExtend -> userExtend.getFarm() == null)
            .map(userExtendMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  Get all the userExtends where Helper is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UserExtendDTO> findAllWhereHelperIsNull() {
        log.debug("Request to get all userExtends where Helper is null");
        return StreamSupport
            .stream(userExtendRepository.findAll().spliterator(), false)
            .filter(userExtend -> userExtend.getHelper() == null)
            .map(userExtendMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one userExtend by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserExtendDTO> findOne(Long id) {
        log.debug("Request to get UserExtend : {}", id);
        return userExtendRepository.findById(id)
            .map(userExtendMapper::toDto);
    }

    /**
     * Delete the userExtend by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserExtend : {}", id);
        userExtendRepository.deleteById(id);
    }

    public Optional<UserExtendDTO> findOneExtendUserByUser(User user){
        return userExtendRepository.findUserExtendByUser(user).map(userExtendMapper::toDto);

    }

    private UserExtend getUserExtend(User user) {
        return userExtendRepository.findUserExtendByUser(user).orElseGet(() -> {
            UserExtend newUserExtend = new UserExtend();
            newUserExtend.setUser(user);
            return newUserExtend;
        });
    }


}
