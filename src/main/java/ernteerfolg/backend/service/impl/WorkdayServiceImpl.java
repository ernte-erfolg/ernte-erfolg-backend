package ernteerfolg.backend.service.impl;

import ernteerfolg.backend.domain.Workday;
import ernteerfolg.backend.service.DateService;
import ernteerfolg.backend.repository.WorkDayRepository;
import ernteerfolg.backend.service.dto.WorkdayDTO;
import ernteerfolg.backend.service.mapper.WorkdayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Workday}.
 */
@Service
@Transactional
public class WorkdayServiceImpl implements DateService {

    private final Logger log = LoggerFactory.getLogger(WorkdayServiceImpl.class);

    private final WorkDayRepository workDayRepository;

    private final WorkdayMapper workdayMapper;

    public WorkdayServiceImpl(WorkDayRepository workDayRepository, WorkdayMapper workdayMapper) {
        this.workDayRepository = workDayRepository;
        this.workdayMapper = workdayMapper;
    }

    /**
     * Save a date.
     *
     * @param workdayDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WorkdayDTO save(WorkdayDTO workdayDTO) {
        log.debug("Request to save Date : {}", workdayDTO);
        Workday workday = workdayMapper.toEntity(workdayDTO);
        workday = workDayRepository.save(workday);
        return workdayMapper.toDto(workday);
    }

    /**
     * Get all the dates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WorkdayDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Dates");
        return workDayRepository.findAll(pageable)
            .map(workdayMapper::toDto);
    }

    /**
     * Get one date by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WorkdayDTO> findOne(Long id) {
        log.debug("Request to get Date : {}", id);
        return workDayRepository.findById(id)
            .map(workdayMapper::toDto);
    }

    /**
     * Delete the date by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Date : {}", id);
        workDayRepository.deleteById(id);
    }
}
