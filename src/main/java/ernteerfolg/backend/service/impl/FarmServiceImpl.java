package ernteerfolg.backend.service.impl;

import ernteerfolg.backend.domain.UserExtend;
import ernteerfolg.backend.service.FarmService;
import ernteerfolg.backend.domain.Farm;
import ernteerfolg.backend.repository.FarmRepository;
import ernteerfolg.backend.service.UserExtendService;
import ernteerfolg.backend.service.dto.FarmDTO;
import ernteerfolg.backend.service.mapper.FarmMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Farm}.
 */
@Service
@Transactional
public class FarmServiceImpl implements FarmService {

    private final Logger log = LoggerFactory.getLogger(FarmServiceImpl.class);

    private final FarmRepository farmRepository;

    private final FarmMapper farmMapper;

    private final UserExtendService userExtendService;

    public FarmServiceImpl(FarmRepository farmRepository, FarmMapper farmMapper, UserExtendService userExtendService) {
        this.farmRepository = farmRepository;
        this.farmMapper = farmMapper;
        this.userExtendService = userExtendService;
    }

    /**
     * Save a farm.
     *
     * @param farmDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FarmDTO save(FarmDTO farmDTO) {
        log.debug("Request to save Farm : {}", farmDTO);
        Farm farm = farmMapper.toEntity(farmDTO);
        UserExtend userExtend = userExtendService.saveFarm(farmDTO);
        farm.setUserExtend(userExtend);
        farm = farmRepository.save(farm);
        return farmMapper.toDto(farm);
    }

    /**
     * Get all the farms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FarmDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Farms");
        return farmRepository.findAll(pageable)
            .map(farmMapper::toDto);
    }

    /**
     * Get one farm by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FarmDTO> findOne(Long id) {
        log.debug("Request to get Farm : {}", id);
        return farmRepository.findById(id)
            .map(farmMapper::toDto);
    }

    /**
     * Delete the farm by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Farm : {}", id);
        farmRepository.deleteById(id);
    }
}
