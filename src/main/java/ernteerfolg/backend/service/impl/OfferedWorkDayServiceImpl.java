package ernteerfolg.backend.service.impl;

import ernteerfolg.backend.service.OfferedWorkDayService;
import ernteerfolg.backend.domain.OfferedWorkDay;
import ernteerfolg.backend.repository.OfferedWorkDayRepository;
import ernteerfolg.backend.service.dto.OfferedWorkDayDTO;
import ernteerfolg.backend.service.mapper.OfferedWorkDayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OfferedWorkDay}.
 */
@Service
@Transactional
public class OfferedWorkDayServiceImpl implements OfferedWorkDayService {

    private final Logger log = LoggerFactory.getLogger(OfferedWorkDayServiceImpl.class);

    private final OfferedWorkDayRepository offeredWorkDayRepository;

    private final OfferedWorkDayMapper offeredWorkDayMapper;

    public OfferedWorkDayServiceImpl(OfferedWorkDayRepository offeredWorkDayRepository, OfferedWorkDayMapper offeredWorkDayMapper) {
        this.offeredWorkDayRepository = offeredWorkDayRepository;
        this.offeredWorkDayMapper = offeredWorkDayMapper;
    }

    /**
     * Save a helperToDate.
     *
     * @param offeredWorkDayDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OfferedWorkDayDTO save(OfferedWorkDayDTO offeredWorkDayDTO) {
        log.debug("Request to save HelperToDate : {}", offeredWorkDayDTO);
        OfferedWorkDay offeredWorkDay = offeredWorkDayMapper.toEntity(offeredWorkDayDTO);
        offeredWorkDay = offeredWorkDayRepository.save(offeredWorkDay);
        return offeredWorkDayMapper.toDto(offeredWorkDay);
    }

    /**
     * Get all the helperToDates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OfferedWorkDayDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HelperToDates");
        return offeredWorkDayRepository.findAll(pageable)
            .map(offeredWorkDayMapper::toDto);
    }




    /**
     * Delete the helperToDate by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HelperToDate : {}", id);
        offeredWorkDayRepository.deleteById(id);
    }
}
