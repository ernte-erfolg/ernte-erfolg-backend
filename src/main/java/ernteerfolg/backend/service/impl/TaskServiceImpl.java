package ernteerfolg.backend.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ernteerfolg.backend.domain.Task;
import ernteerfolg.backend.repository.TaskRepository;
import ernteerfolg.backend.service.TaskService;
import ernteerfolg.backend.service.dto.TasksInAreaDTO;

/**
 * Service Implementation for managing {@link Task}.
 */
@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    private final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    private int[][] searchDistanceAreas = {{0,30},{31,50},{51,70},{71,150},{151,500}};
    
	private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }
    
    /**
     * Searches for Tasks in areas. The areas to search are specified in {@link #searchDistanceAreas}.
     * @param lat latitude of the place from which to search
     * @param lon longitude of the place from which to search
     * @return Zero, one or two TasksInAreaDTO Objects in a list. 
     * @see TaskServiceImplTest   
     */
    public List<TasksInAreaDTO> searchTasks(double lat, double lon) throws TransformException {
    
    	List<TasksInAreaDTO> result = new ArrayList<>();
    	List<Task> allTasks = taskRepository.findAll();

    	int area = 0;
    	while((result.size() < 2) && area < searchDistanceAreas.length)  {
    		int minDistance = searchDistanceAreas[area][0];
    		int maxDistance = searchDistanceAreas[area][1];
    		area++;
    		Map<Task,Double> taskAndDistanceMap = searchTasks(allTasks, lat, lon, minDistance, maxDistance);
    		log.debug("tasks in distance " + minDistance + " to " + maxDistance + " size: " + taskAndDistanceMap.size());
    		
    		
    		if(!taskAndDistanceMap.isEmpty()) {
    			TasksInAreaDTO tasksInArea = new TasksInAreaDTO(maxDistance);
    			for (Entry<Task,Double> entry  : taskAndDistanceMap.entrySet()) {
    				tasksInArea.add(entry.getKey(), entry.getValue());
				}
    			result.add(tasksInArea);
    		}
    	}
    	    	
    	return result;
    }    
   
    /**
     * Filtering the passed tasks by tasks that are located in the specified area.
     * 
     * @param tasks all tasks to search/filter.
     * @param lat latitude of the base coordinate
     * @param lon longitude of the base coordinate
     * @param minDistance the minimal distance of the area meassured from the base coordinate.
     * @param maxDistance the maximuim distance of the area meassured from the base coordinate.
     * @return a map with task,distance entries.
     * @throws TransformException
     */
    private Map<Task,Double> searchTasks(List<Task> tasks, double lat, double lon, double minDistance, double maxDistance) throws TransformException {

    	Map<Task,Double> taskAndDistanceMap = new HashMap<>();
    	
    	for (Task task : tasks) {
    		
    		if( task.getLat() != null && task.getLon() != null ) {
    			
    			double calculatedDistance = getDistanceInKilometers(task.getLat().doubleValue(), task.getLon().doubleValue(), lat, lon);
    			log.trace("calculatedDistance " + calculatedDistance );
    			if(calculatedDistance >= minDistance && calculatedDistance <= maxDistance) {
    				taskAndDistanceMap.put(task, calculatedDistance);
    			}
    		}
		}
    	return taskAndDistanceMap;
    }

    /**
     * Gets the distance between two coordinates in kilometers.
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return Distance between two coordinates in kilometers.
     * @throws TransformException
     */
	private double getDistanceInKilometers(double lat1, double lon1, double lat2, double lon2) throws TransformException {
		double distance = -1.0;

		try {
			GeodeticCalculator gc = new GeodeticCalculator();
			gc.setStartingPosition(new DirectPosition2D(lat1, lon1));
			gc.setDestinationPosition(new DirectPosition2D(lat2, lon2));

			distance = gc.getOrthodromicDistance();
		} catch (Exception e) {
			log.warn(String.format("Problems calculating the distance lat1={%s}, lon1={%s}, lat2={%s}, lon2={%s} - {%s}", lat1, lon1, lat2, lon2, e.getMessage()));
		}

        return distance/1000;
	}

	public void setSearchDistanceAreas(int[][] searchDistanceAreas) {
		this.searchDistanceAreas = searchDistanceAreas;
	}
}
