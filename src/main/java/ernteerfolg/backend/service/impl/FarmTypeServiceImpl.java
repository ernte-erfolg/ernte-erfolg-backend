package ernteerfolg.backend.service.impl;

import ernteerfolg.backend.service.FarmTypeService;
import ernteerfolg.backend.domain.FarmType;
import ernteerfolg.backend.repository.FarmTypeRepository;
import ernteerfolg.backend.service.dto.FarmTypeDTO;
import ernteerfolg.backend.service.mapper.FarmTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link FarmType}.
 */
@Service
@Transactional
public class FarmTypeServiceImpl implements FarmTypeService {

    private final Logger log = LoggerFactory.getLogger(FarmTypeServiceImpl.class);

    private final FarmTypeRepository farmTypeRepository;

    private final FarmTypeMapper farmTypeMapper;

    public FarmTypeServiceImpl(FarmTypeRepository farmTypeRepository, FarmTypeMapper farmTypeMapper) {
        this.farmTypeRepository = farmTypeRepository;
        this.farmTypeMapper = farmTypeMapper;
    }

    /**
     * Save a farmType.
     *
     * @param farmTypeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FarmTypeDTO save(FarmTypeDTO farmTypeDTO) {
        log.debug("Request to save FarmType : {}", farmTypeDTO);
        FarmType farmType = farmTypeMapper.toEntity(farmTypeDTO);
        farmType = farmTypeRepository.save(farmType);
        return farmTypeMapper.toDto(farmType);
    }

    /**
     * Get all the farmTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FarmTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FarmTypes");
        return farmTypeRepository.findAll(pageable)
            .map(farmTypeMapper::toDto);
    }

    /**
     * Get one farmType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FarmTypeDTO> findOne(Long id) {
        log.debug("Request to get FarmType : {}", id);
        return farmTypeRepository.findById(id)
            .map(farmTypeMapper::toDto);
    }

    /**
     * Delete the farmType by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FarmType : {}", id);
        farmTypeRepository.deleteById(id);
    }
}
