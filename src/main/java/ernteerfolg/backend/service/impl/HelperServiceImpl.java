package ernteerfolg.backend.service.impl;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ernteerfolg.backend.domain.*;
import ernteerfolg.backend.service.mapper.UserExtendMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ernteerfolg.backend.repository.HelperRepository;
import ernteerfolg.backend.repository.OfferedWorkDayRepository;
import ernteerfolg.backend.repository.TaskRepository;
import ernteerfolg.backend.repository.UserExtendRepository;
import ernteerfolg.backend.repository.UserRepository;
import ernteerfolg.backend.repository.WorkDayRepository;
import ernteerfolg.backend.security.SecurityUtils;
import ernteerfolg.backend.service.AddressService;
import ernteerfolg.backend.service.HelperService;
import ernteerfolg.backend.service.dto.HelperDTO;
import ernteerfolg.backend.service.dto.OfferDTO;
import ernteerfolg.backend.service.mapper.HelperMapper;
import ernteerfolg.backend.service.mapper.OfferedWorkDayMapper;
import ernteerfolg.backend.service.mapper.WorkdayMapper;

/**
 * Service Implementation for managing {@link Helper}.
 */
@Service
@Transactional
public class HelperServiceImpl implements HelperService {

    private final Logger log = LoggerFactory.getLogger(HelperServiceImpl.class);

    private final HelperRepository helperRepository;

    private final UserExtendServiceImpl userExtendService;

    private final UserExtendRepository userExtendRepository;

    private final UserExtendMapper userExtendMapper;

    private final UserRepository userRepository;

    private final HelperMapper helperMapper;

    private final AddressService addressService;

    private final TaskRepository taskRepository;

    private final WorkDayRepository workDayRepository;

    private final WorkdayMapper workdayMapper;

    private final OfferedWorkDayMapper offeredWorkDayMapper;

    private final OfferedWorkDayRepository offeredWorkDayRepository;

    public HelperServiceImpl(HelperRepository helperRepository, UserExtendServiceImpl userExtendService, UserExtendRepository userExtendRepository, UserExtendMapper userExtendMapper, UserRepository userRepository, HelperMapper helperMapper, AddressService addressService, TaskRepository taskRepository, WorkDayRepository workDayRepository, WorkdayMapper workdayMapper, OfferedWorkDayMapper offeredWorkDayMapper, OfferedWorkDayRepository offeredWorkDayRepository) {
        this.helperRepository = helperRepository;
        this.userExtendService = userExtendService;
        this.userExtendMapper = userExtendMapper;
        this.workDayRepository = workDayRepository;
        this.userExtendRepository = userExtendRepository;
        this.userRepository = userRepository;
        this.helperMapper = helperMapper;
        this.addressService = addressService;
        this.taskRepository = taskRepository;
        this.workdayMapper = workdayMapper;
        this.offeredWorkDayMapper = offeredWorkDayMapper;
        this.offeredWorkDayRepository = offeredWorkDayRepository;
    }

    /**
     * Save a helper.
     *
     * @param helperDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HelperDTO save(HelperDTO helperDTO) {
        log.debug("Request to save Helper : {}", helperDTO);
        UserExtend userExtend = userExtendService.saveHelper(helperDTO);
        Helper helper = helperMapper.toEntity(helperDTO);
        helper.setUserExtend(userExtend);
        helper = helperRepository.save(helper);
        return helperMapper.toDto(helper);
    }

    /**
     * Get all the helpers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HelperDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Helpers");
        return helperRepository.findAll(pageable)
            .map(helperMapper::toDto);
    }

    /**
     * Get one helper by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HelperDTO> findOne(Long id) {
        log.debug("Request to get Helper : {}", id);
        return helperRepository.findById(id)
            .map(helperMapper::toDto);
    }

    /**
     * Delete the helper by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Helper : {}", id);
        helperRepository.deleteById(id);
    }

    public Set<OfferDTO> getMyOffers(){
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();
        Helper helper = userExtendRepository.findUserExtendByUser(user).get().getHelper();

        Set<OfferedWorkDay> workdays = helper.getOfferedWorkDays();
        if(workdays == null) return null;
        Set<OfferDTO> myOffers = workdays.stream().map(OfferedWorkDay::getWorkdays).filter(Objects::nonNull).map(Workday::getTasks)
            .map(tasks -> tasks.stream()
                .map(Task::getId))
            .reduce(Stream::concat)
            .orElseGet(Stream::empty)
            .map(this::getMyOfferForTask)
            .collect(Collectors.toSet());
        return myOffers;

    }

   public OfferDTO getMyOfferForTask(Long taskId){
       User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();
        Helper helper = userExtendRepository.findUserExtendByUser(user).get().getHelper();
        Task task = taskRepository.getOne(taskId);
        OfferDTO offerDTO = new OfferDTO();
        offerDTO.setTaskId(task.getId());
        offerDTO.setFarmId(task.getFarm().getId());
        offerDTO.setFarmName(task.getFarm().getName());
        Set<Workday> workdays = task.getHelpers();
        offerDTO.setRequestedDays(
            workdays.stream().map(workdayMapper::toDto).collect(Collectors.toSet())

        );
        Set<OfferedWorkDay> offeredWorkDay = offeredWorkDayRepository.findAllByHelperAndWorkdayIn(helper, workdays);
        offerDTO.setMyGuaranteedDays(offeredWorkDay
            .stream()
            .map(offeredWorkDayMapper::toDto)
            .collect(Collectors.toSet()));

       offerDTO.setGuaranteedDays(
           offeredWorkDayRepository
               .findAllByWorkdayIn(workdays)
               .stream().map(offeredWorkDayMapper::toDto).peek(offeredWorkDayDTO -> {
                   offeredWorkDayDTO.setHelperId(null);
                   offeredWorkDayDTO.setSuccess(null);
           }).collect(Collectors.toSet()));
    return offerDTO;
    }

    public OfferDTO setOfferForTask(OfferDTO offerDTO){
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();
        Helper helper = userExtendRepository.findUserExtendByUser(user).get().getHelper();

        Set<OfferedWorkDay> offeredWorkDays = offerDTO.getMyGuaranteedDays()
            .stream()
            .map(offeredWorkDayMapper::toEntity)
            .peek(offeredWorkDay -> offeredWorkDay.setHelper(helper))
            .collect(Collectors.toSet());

        offeredWorkDays = offeredWorkDays.stream().map(offeredWorkDayRepository::save).collect(Collectors.toSet());
        helper.setOfferedWorkDays(offeredWorkDays);
        helperRepository.save(helper.offeredWorkDays(offeredWorkDays));
        return getMyOfferForTask(offerDTO.getTaskId());
    }

}
