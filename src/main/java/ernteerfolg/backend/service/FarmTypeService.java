package ernteerfolg.backend.service;

import ernteerfolg.backend.service.dto.FarmTypeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ernteerfolg.backend.domain.FarmType}.
 */
public interface FarmTypeService {

    /**
     * Save a farmType.
     *
     * @param farmTypeDTO the entity to save.
     * @return the persisted entity.
     */
    FarmTypeDTO save(FarmTypeDTO farmTypeDTO);

    /**
     * Get all the farmTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FarmTypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" farmType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FarmTypeDTO> findOne(Long id);

    /**
     * Delete the "id" farmType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
