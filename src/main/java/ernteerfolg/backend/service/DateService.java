package ernteerfolg.backend.service;

import ernteerfolg.backend.domain.Workday;
import ernteerfolg.backend.service.dto.WorkdayDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Workday}.
 */
public interface DateService {

    /**
     * Save a date.
     *
     * @param workdayDTO the entity to save.
     * @return the persisted entity.
     */
    WorkdayDTO save(WorkdayDTO workdayDTO);

    /**
     * Get all the dates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WorkdayDTO> findAll(Pageable pageable);

    /**
     * Get the "id" date.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WorkdayDTO> findOne(Long id);

    /**
     * Delete the "id" date.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
