package ernteerfolg.backend.service.plz;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonMappingException;

import ernteerfolg.backend.service.MailService;

@Service
public class ZipGeoService {

	private final Logger log = LoggerFactory.getLogger(ZipGeoService.class);
	private Map<String,ZipGeoDataSet> plzDataSetMap;
	
	public Map<String, ZipGeoDataSet> getPlzDataSetMap() {
		return plzDataSetMap;
	}

	public ZipGeoService() {
		plzDataSetMap = new HashMap<String, ZipGeoDataSet>();
		parsePlzFile();
	}
	
	public Optional<ZipGeoDataSet> findByZip(String zip) {
		Optional<ZipGeoDataSet> result = Optional.empty();
		if(StringUtils.isNotBlank(zip)) {			
			ZipGeoDataSet zipGeoDataSet = this.plzDataSetMap.get(zip);
			log.debug("zipGeoDataSet for " + zip + ":" + zipGeoDataSet);
			result = Optional.ofNullable(zipGeoDataSet);
		}
		else {
			log.debug("the passed zip is blank!");
		}
			
		return result;
	}
	
	private void parsePlzFile() {
		ClassPathResource json = new ClassPathResource("jsonDatasources/postleitzahlen-deutschland.json");

		try (JsonParser jParser = new JsonFactory().createParser(json.getInputStream());) {
			
			if (jParser.nextToken() == JsonToken.START_ARRAY) {
				ZipGeoDataSet plzDataSet = new ZipGeoDataSet();
				while (jParser.nextToken() != JsonToken.END_ARRAY) {

					while (jParser.nextToken() != JsonToken.END_OBJECT) {

						String fieldname = jParser.getCurrentName();

						if ("datasetid".equals(fieldname)) {
							jParser.nextToken();
						}

						if ("recordid".equals(fieldname)) {
							jParser.nextToken();
							plzDataSet.setRecordid(jParser.getText());
						}

						if ("fields".equals(fieldname)) {
								gotoTokenValue(jParser, "note");
								plzDataSet.setCountry(jParser.getText());
								parseCoordinates(jParser, plzDataSet);								
								gotoTokenValue(jParser, "plz");
								plzDataSet.setZip(jParser.getText());
								while (jParser.nextToken() != JsonToken.END_OBJECT) {}
								while (jParser.nextToken() != JsonToken.END_OBJECT) {}
						}
					}
					plzDataSetMap.put(plzDataSet.getZip(), plzDataSet);
					plzDataSet = new ZipGeoDataSet();
				}
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private  void parseCoordinates(JsonParser jParser, ZipGeoDataSet plzDataSet) throws IOException {
		gotoTokenValue(jParser, "geo_point_2d");
		jParser.nextToken();
		plzDataSet.setLatitude(new BigDecimal(jParser.getText()));
		jParser.nextToken();
		plzDataSet.setLongitude(new BigDecimal(jParser.getText()));
	}

	private  void gotoTokenValue(JsonParser jParser, String fieldname) throws IOException {
		while (true) {
			jParser.nextToken();
			if (fieldname.equals(jParser.getCurrentName())){
				jParser.nextToken();
				break;
			}
		}
	}
}
