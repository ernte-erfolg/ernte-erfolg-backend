package ernteerfolg.backend.service;

import ernteerfolg.backend.domain.OfferedWorkDay;
import ernteerfolg.backend.service.dto.OfferedWorkDayDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link OfferedWorkDay}.
 */
public interface OfferedWorkDayService {

    /**
     * Save a helperToDate.
     *
     * @param offeredWorkDayDTO the entity to save.
     * @return the persisted entity.
     */
    OfferedWorkDayDTO save(OfferedWorkDayDTO offeredWorkDayDTO);

    /**
     * Get all the helperToDates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OfferedWorkDayDTO> findAll(Pageable pageable);


    /**
     * Delete the "id" helperToDate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
