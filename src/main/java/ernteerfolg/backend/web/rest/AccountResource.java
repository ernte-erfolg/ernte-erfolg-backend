package ernteerfolg.backend.web.rest;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ernteerfolg.backend.domain.User;
import ernteerfolg.backend.domain.UserExtend;
import ernteerfolg.backend.domain.enumeration.UserClass;
import ernteerfolg.backend.repository.UserExtendRepository;
import ernteerfolg.backend.service.MailService;
import ernteerfolg.backend.service.UserService;
import ernteerfolg.backend.service.dto.PasswordChangeDTO;
import ernteerfolg.backend.service.dto.UserDTO;
import ernteerfolg.backend.service.mapper.UserMapper;
import ernteerfolg.backend.web.rest.errors.BadRequestAlertException;
import ernteerfolg.backend.web.rest.errors.EmailAlreadyUsedException;
import ernteerfolg.backend.web.rest.errors.InvalidPasswordException;
import ernteerfolg.backend.web.rest.errors.LoginAlreadyUsedException;
import ernteerfolg.backend.web.rest.vm.KeyAndPasswordVM;
import ernteerfolg.backend.web.rest.vm.ManagedUserVM;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private static class AccountResourceException extends RuntimeException {
        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserExtendRepository userExtendRepository;

    private final UserService userService;

    private final MailService mailService;

    private final UserMapper userMapper;

    public AccountResource(UserExtendRepository userExtendRepository, UserService userService, MailService mailService, UserMapper userMapper) {

    	this.userExtendRepository = userExtendRepository;
        this.userService = userService;
        this.mailService = mailService;
        this.userMapper = userMapper;		
    }

    /**
     * {@code GET  /resendConfirmationMail} : resend the confirmation mail
     *
     * @param The email of that was used for registration.
     * @throws BadRequestAlertException{@code 400 (Bad Request)} if there is no registartion for the passed email.
     * @throws BadRequestAlertException{@code 400 (Bad Request)} if the user for the passed email is already activated.
     */
    @GetMapping("/resendConfirmationMail")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void resendConfirmationMail(@RequestParam(value = "email")  String email) {

    	log.debug("REST request to resend a confirmation mail for " + email);
    	Optional<User> userOptional = userService.getUserByEmail(email);
    	
    	if( !userOptional.isPresent() ) {
    		throw new BadRequestAlertException("Unknown user.", "User", "unknownUser");
    	}

    	if( userOptional.isPresent() && userOptional.get().getActivated() ) {
    		throw new BadRequestAlertException("User already activated", "User", "alreadyActivated");
    	}
    	
    	mailService.sendActivationEmail(userOptional.get());
    }

    
    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        if(managedUserVM.getLangKey()== null){
            throw new BadRequestAlertException("Invalid LangKey", "ManagedUserVM", "LangKeyNull");
        }
        if( managedUserVM.getUserClass() == null) {
        	throw new BadRequestAlertException("Invalid userClass", "ManagedUserVM", "userClassNull");
        }
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        
        UserExtend userExtend = new UserExtend();
        userExtend.setUserClass(managedUserVM.getUserClass());
        userExtend.setUser(user);
        userExtend = userExtendRepository.save(userExtend);
        
        mailService.sendActivationEmail(user);
        UserDTO result = userMapper.userToUserDTO(user);
        result.setUserClass(userExtend.getUserClass());
        return result;
    }

    /**
     * {@code GET  /activate} : activate the registered user.
     *
     * @param key the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
     */
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )     
    public UserDTO getAccount() {
    	Optional<User> userOptional = userService.getUserWithAuthorities();
        UserDTO userDTO = userOptional
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
        
        userExtendRepository.findUserExtendByUser(userOptional.get())
        	.ifPresent(userExtend -> userDTO.setUserClass(UserClass.valueOf(userExtend.getUserClass().name())));
        
        return userDTO;
    }

//    /**
//     * {@code POST  /account} : update the current user information.
//     *
//     * @param userDTO the current user information.
//     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
//     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user login wasn't found.
//     */
//    @PostMapping("/account")
//    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
//    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
//        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new AccountResourceException("Current user login not found"));
//        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
//        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
//            throw new EmailAlreadyUsedException();
//        }
//        Optional<User> user = userRepository.findOneByLogin(userLogin);
//        if (!user.isPresent()) {
//            throw new AccountResourceException("User could not be found");
//        }
//        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
//            userDTO.getLangKey(), userDTO.getImageUrl());
//    }

    /**
     * {@code POST  /account/change-password} : changes the current user's password.
     *
     * @param passwordChangeDto current and new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the new password is incorrect.
     */
    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }

    /**
     * {@code POST   /account/reset-password/init} : Send an email to reset the password of the user.
     *
     * @param mail the mail of the user.
     */
    @PostMapping(path = "/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
        Optional<User> user = userService.requestPasswordReset(mail);
        if (user.isPresent()) {
            mailService.sendPasswordResetMail(user.get());
        } else {
            // Pretend the request has been successful to prevent checking which emails really exist
            // but log that an invalid attempt has been made
            log.warn("Password reset requested for non existing mail '{}'", mail);
        }
    }

    /**
     * {@code POST   /account/reset-password/finish} : Finish to reset the password of the user.
     *
     * @param keyAndPassword the generated key and the new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the password could not be reset.
     */
    @PostMapping(path = "/account/reset-password/finish")
    public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user =
            userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this reset key");
        }
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }
}
