package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.service.FarmTypeService;
import ernteerfolg.backend.web.rest.errors.BadRequestAlertException;
import ernteerfolg.backend.service.dto.FarmTypeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ernteerfolg.backend.domain.FarmType}.
 */
//@RestController
//@RequestMapping("/api")
public class FarmTypeResource {

//    private final Logger log = LoggerFactory.getLogger(FarmTypeResource.class);
//
//    private static final String ENTITY_NAME = "farmType";
//
//    @Value("${jhipster.clientApp.name}")
//    private String applicationName;
//
//    private final FarmTypeService farmTypeService;
//
//    public FarmTypeResource(FarmTypeService farmTypeService) {
//        this.farmTypeService = farmTypeService;
//    }
//
//    /**
//     * {@code POST  /farm-types} : Create a new farmType.
//     *
//     * @param farmTypeDTO the farmTypeDTO to create.
//     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new farmTypeDTO, or with status {@code 400 (Bad Request)} if the farmType has already an ID.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PostMapping("/farm-types")
//    public ResponseEntity<FarmTypeDTO> createFarmType(@RequestBody FarmTypeDTO farmTypeDTO) throws URISyntaxException {
//        log.debug("REST request to save FarmType : {}", farmTypeDTO);
//        if (farmTypeDTO.getId() != null) {
//            throw new BadRequestAlertException("A new farmType cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        FarmTypeDTO result = farmTypeService.save(farmTypeDTO);
//        return ResponseEntity.created(new URI("/api/farm-types/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }
//
//    /**
//     * {@code PUT  /farm-types} : Updates an existing farmType.
//     *
//     * @param farmTypeDTO the farmTypeDTO to update.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated farmTypeDTO,
//     * or with status {@code 400 (Bad Request)} if the farmTypeDTO is not valid,
//     * or with status {@code 500 (Internal Server Error)} if the farmTypeDTO couldn't be updated.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PutMapping("/farm-types")
//    public ResponseEntity<FarmTypeDTO> updateFarmType(@RequestBody FarmTypeDTO farmTypeDTO) throws URISyntaxException {
//        log.debug("REST request to update FarmType : {}", farmTypeDTO);
//        if (farmTypeDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        FarmTypeDTO result = farmTypeService.save(farmTypeDTO);
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, farmTypeDTO.getId().toString()))
//            .body(result);
//    }
//
//    /**
//     * {@code GET  /farm-types} : get all the farmTypes.
//     *
//     * @param pageable the pagination information.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of farmTypes in body.
//     */
//    @GetMapping("/farm-types")
//    public ResponseEntity<List<FarmTypeDTO>> getAllFarmTypes(Pageable pageable) {
//        log.debug("REST request to get a page of FarmTypes");
//        Page<FarmTypeDTO> page = farmTypeService.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }
//
//    /**
//     * {@code GET  /farm-types/:id} : get the "id" farmType.
//     *
//     * @param id the id of the farmTypeDTO to retrieve.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the farmTypeDTO, or with status {@code 404 (Not Found)}.
//     */
//    @GetMapping("/farm-types/{id}")
//    public ResponseEntity<FarmTypeDTO> getFarmType(@PathVariable Long id) {
//        log.debug("REST request to get FarmType : {}", id);
//        Optional<FarmTypeDTO> farmTypeDTO = farmTypeService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(farmTypeDTO);
//    }
//
//    /**
//     * {@code DELETE  /farm-types/:id} : delete the "id" farmType.
//     *
//     * @param id the id of the farmTypeDTO to delete.
//     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
//     */
//    @DeleteMapping("/farm-types/{id}")
//    public ResponseEntity<Void> deleteFarmType(@PathVariable Long id) {
//        log.debug("REST request to delete FarmType : {}", id);
//        farmTypeService.delete(id);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
//    }
}
