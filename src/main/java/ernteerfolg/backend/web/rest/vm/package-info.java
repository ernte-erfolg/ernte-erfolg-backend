/**
 * View Models used by Spring MVC REST controllers.
 */
package ernteerfolg.backend.web.rest.vm;
