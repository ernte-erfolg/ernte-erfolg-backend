package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.domain.OfferedWorkDay;
import ernteerfolg.backend.service.OfferedWorkDayService;
import ernteerfolg.backend.service.dto.OfferedWorkDayDTO;
import ernteerfolg.backend.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link OfferedWorkDay}.
 */
@RestController
@RequestMapping("/api")
public class OfferedWorkDayResource {

    private final Logger log = LoggerFactory.getLogger(OfferedWorkDayResource.class);

    private static final String ENTITY_NAME = "helperToDate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OfferedWorkDayService offeredWorkDayService;

    public OfferedWorkDayResource(OfferedWorkDayService offeredWorkDayService) {
        this.offeredWorkDayService = offeredWorkDayService;
    }

    /**
     * {@code POST  /helper-to-dates} : Create a new helperToDate.
     *
     * @param offeredWorkDayDTO the helperToDateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new helperToDateDTO, or with status {@code 400 (Bad Request)} if the helperToDate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/helper-to-dates")
    public ResponseEntity<OfferedWorkDayDTO> createHelperToDate(@RequestBody OfferedWorkDayDTO offeredWorkDayDTO) throws URISyntaxException {
        log.debug("REST request to save HelperToDate : {}", offeredWorkDayDTO);
        if (offeredWorkDayDTO.getId() != null) {
            throw new BadRequestAlertException("A new helperToDate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OfferedWorkDayDTO result = offeredWorkDayService.save(offeredWorkDayDTO);
        return ResponseEntity.created(new URI("/api/helper-to-dates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /helper-to-dates} : Updates an existing helperToDate.
     *
     * @param offeredWorkDayDTO the helperToDateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated helperToDateDTO,
     * or with status {@code 400 (Bad Request)} if the helperToDateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the helperToDateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/helper-to-dates")
    public ResponseEntity<OfferedWorkDayDTO> updateHelperToDate(@RequestBody OfferedWorkDayDTO offeredWorkDayDTO) throws URISyntaxException {
        log.debug("REST request to update HelperToDate : {}", offeredWorkDayDTO);
        if (offeredWorkDayDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OfferedWorkDayDTO result = offeredWorkDayService.save(offeredWorkDayDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, offeredWorkDayDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code DELETE  /helper-to-dates/:id} : delete the "id" helperToDate.
     *
     * @param id the id of the helperToDateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/helper-to-dates/{id}")
    public ResponseEntity<Void> deleteHelperToDate(@PathVariable Long id) {
        log.debug("REST request to delete HelperToDate : {}", id);
        offeredWorkDayService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

}
