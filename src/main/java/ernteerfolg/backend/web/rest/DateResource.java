package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.domain.Workday;

/**
 * REST controller for managing {@link Workday}.
 */
//@RestController
//@RequestMapping("/api")
public class DateResource {

//    private final Logger log = LoggerFactory.getLogger(DateResource.class);
//
//    private static final String ENTITY_NAME = "date";
//
//    @Value("${jhipster.clientApp.name}")
//    private String applicationName;
//
//    private final DateService dateService;
//
//    public DateResource(DateService dateService) {
//        this.dateService = dateService;
//    }
//
//    /**
//     * {@code POST  /dates} : Create a new date.
//     *
//     * @param dateDTO the dateDTO to create.
//     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dateDTO, or with status {@code 400 (Bad Request)} if the date has already an ID.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PostMapping("/dates")
//    public ResponseEntity<DateDTO> createDate(@RequestBody DateDTO dateDTO) throws URISyntaxException {
//        log.debug("REST request to save Date : {}", dateDTO);
//        if (dateDTO.getId() != null) {
//            throw new BadRequestAlertException("A new date cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        DateDTO result = dateService.save(dateDTO);
//        return ResponseEntity.created(new URI("/api/dates/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }
//
//    /**
//     * {@code PUT  /dates} : Updates an existing date.
//     *
//     * @param dateDTO the dateDTO to update.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dateDTO,
//     * or with status {@code 400 (Bad Request)} if the dateDTO is not valid,
//     * or with status {@code 500 (Internal Server Error)} if the dateDTO couldn't be updated.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PutMapping("/dates")
//    public ResponseEntity<DateDTO> updateDate(@RequestBody DateDTO dateDTO) throws URISyntaxException {
//        log.debug("REST request to update Date : {}", dateDTO);
//        if (dateDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        DateDTO result = dateService.save(dateDTO);
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dateDTO.getId().toString()))
//            .body(result);
//    }
//
//    /**
//     * {@code GET  /dates} : get all the dates.
//     *
//     * @param pageable the pagination information.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dates in body.
//     */
//    @GetMapping("/dates")
//    public ResponseEntity<List<DateDTO>> getAllDates(Pageable pageable) {
//        log.debug("REST request to get a page of Dates");
//        Page<DateDTO> page = dateService.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }
//
//    /**
//     * {@code GET  /dates/:id} : get the "id" date.
//     *
//     * @param id the id of the dateDTO to retrieve.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dateDTO, or with status {@code 404 (Not Found)}.
//     */
//    @GetMapping("/dates/{id}")
//    public ResponseEntity<DateDTO> getDate(@PathVariable Long id) {
//        log.debug("REST request to get Date : {}", id);
//        Optional<DateDTO> dateDTO = dateService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(dateDTO);
//    }
//
//    /**
//     * {@code DELETE  /dates/:id} : delete the "id" date.
//     *
//     * @param id the id of the dateDTO to delete.
//     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
//     */
//    @DeleteMapping("/dates/{id}")
//    public ResponseEntity<Void> deleteDate(@PathVariable Long id) {
//        log.debug("REST request to delete Date : {}", id);
//        dateService.delete(id);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
//    }
}
