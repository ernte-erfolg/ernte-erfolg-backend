package ernteerfolg.backend.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.HeaderUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * REST controller for managing {@link ernteerfolg.backend.domain.Task}.
 */
@RestController
@RequestMapping("/api")
public class VersionResource {

    private final Logger log = LoggerFactory.getLogger(VersionResource.class);

    private static final String ENTITY_NAME = "version";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public VersionResource() {
    }

    /**
     * {@code GET  /vesrion} : get the version.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the version.
     */
    @GetMapping("/version")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<String> getVersion() throws URISyntaxException {
    	
    	log.debug("REST request to get Version : {}");

    	String version = "not detected"; 
    	ClassPathResource cpr = new ClassPathResource("META-INF/maven/ernteerfolg.backend/ernte-erfolg-backend/pom.properties");
    	Properties p = new Properties();
    	try {
			p.load(cpr.getInputStream());
			version = p.getProperty("version");
		} catch (java.io.IOException e) {
			// ignore
		}
    	
        return ResponseEntity.created(new URI("/api/version/" + version))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, version))
            .body(version);
    }
}
