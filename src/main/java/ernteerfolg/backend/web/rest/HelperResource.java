package ernteerfolg.backend.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ernteerfolg.backend.repository.TaskRepository;
import ernteerfolg.backend.service.HelperService;
import ernteerfolg.backend.service.dto.HelperDTO;
import ernteerfolg.backend.service.dto.OfferDTO;
import ernteerfolg.backend.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * REST controller for managing {@link ernteerfolg.backend.domain.Helper}.
 */
@RestController
@RequestMapping("/api")
public class HelperResource {

    private final Logger log = LoggerFactory.getLogger(HelperResource.class);

    private static final String ENTITY_NAME = "helper";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HelperService helperService;

	private TaskRepository taskRepository;


    public HelperResource(HelperService helperService, TaskRepository taskRepository) {
        this.helperService = helperService;
		this.taskRepository = taskRepository;
    }

    /**
     * {@code POST  /helpers} : Create a new helper.
     *
     * @param helperDTO the helperDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new helperDTO, or with status {@code 400 (Bad Request)} if the helper has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/helpers")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<HelperDTO> createHelper(@RequestBody HelperDTO helperDTO) throws URISyntaxException {
        log.debug("REST request to save Helper : {}", helperDTO);
        if (helperDTO.getId() != null) {
            throw new BadRequestAlertException("A new helper cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HelperDTO result = helperService.save(helperDTO);
        return ResponseEntity.created(new URI("/api/helpers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /helpers} : Updates an existing helper.
     *
     * @param helperDTO the helperDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated helperDTO,
     * or with status {@code 400 (Bad Request)} if the helperDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the helperDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/helpers")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<HelperDTO> updateHelper(@RequestBody HelperDTO helperDTO) throws URISyntaxException {
        log.debug("REST request to update Helper : {}", helperDTO);
        if (helperDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HelperDTO result = helperService.save(helperDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, helperDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /helpers} : get all the helpers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of helpers in body.
     */
    @GetMapping("/helpers")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<List<HelperDTO>> getAllHelpers(Pageable pageable) {
        log.debug("REST request to get a page of Helpers");
        Page<HelperDTO> page = helperService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /helpers/:id} : get the "id" helper.
     *
     * @param id the id of the helperDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the helperDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/helpers/{id}")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<HelperDTO> getHelper(@PathVariable Long id) {
        log.debug("REST request to get Helper : {}", id);
        Optional<HelperDTO> helperDTO = helperService.findOne(id);
        return ResponseUtil.wrapOrNotFound(helperDTO);
    }

    /**
     * {@code DELETE  /helpers/:id} : delete the "id" helper.
     *
     * @param id the id of the helperDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/helpers/{id}")
    public ResponseEntity<Void> deleteHelper(@PathVariable Long id) {
        log.debug("REST request to delete Helper : {}", id);
        helperService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("helpers/offer/task_id/{task_id}")
    public ResponseEntity<OfferDTO> getMyOfferForFarm(@PathVariable (value = "task_id") Long taskId){
        if(!taskRepository.existsById(taskId)){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body( helperService.getMyOfferForTask(taskId));
    }

    @PostMapping("helpers/offer")
    public ResponseEntity<OfferDTO> createMyOffersForFarm(@RequestBody OfferDTO offerDTO){
        return ResponseEntity.ok(helperService.setOfferForTask(offerDTO));
    }
    @GetMapping("helpers/offer")
    public ResponseEntity<Set<OfferDTO>> getMyOffers(){
        return ResponseEntity.ok(helperService.getMyOffers());
    }

}
