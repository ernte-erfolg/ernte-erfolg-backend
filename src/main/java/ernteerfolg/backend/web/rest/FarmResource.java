package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.service.FarmService;
import ernteerfolg.backend.web.rest.errors.BadRequestAlertException;
import ernteerfolg.backend.service.dto.FarmDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ernteerfolg.backend.domain.Farm}.
 */
@RestController
@RequestMapping("/api")
public class FarmResource {

    private final Logger log = LoggerFactory.getLogger(FarmResource.class);

    private static final String ENTITY_NAME = "farm";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FarmService farmService;

    public FarmResource(FarmService farmService) {
        this.farmService = farmService;
    }

    /**
     * {@code POST  /farms} : Create a new farm.
     *
     * @param farmDTO the farmDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new farmDTO, or with status {@code 400 (Bad Request)} if the farm has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/farms")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<FarmDTO> createFarm(@RequestBody FarmDTO farmDTO) throws URISyntaxException {
        log.debug("REST request to save Farm : {}", farmDTO);
        if (farmDTO.getId() != null) {
            throw new BadRequestAlertException("A new farm cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FarmDTO result = farmService.save(farmDTO);
        return ResponseEntity.created(new URI("/api/farms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /farms} : Updates an existing farm.
     *
     * @param farmDTO the farmDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated farmDTO,
     * or with status {@code 400 (Bad Request)} if the farmDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the farmDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/farms")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<FarmDTO> updateFarm(@RequestBody FarmDTO farmDTO) throws URISyntaxException {
        log.debug("REST request to update Farm : {}", farmDTO);
        if (farmDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FarmDTO result = farmService.save(farmDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, farmDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /farms} : get all the farms.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of farms in body.
     */
    @GetMapping("/farms")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<List<FarmDTO>> getAllFarms(Pageable pageable) {
        log.debug("REST request to get a page of Farms");
        Page<FarmDTO> page = farmService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

        /**
         * {@code GET  /farms/:id} : get the "id" farm.
         *
         * @param id the id of the farmDTO to retrieve.
         * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the farmDTO, or with status {@code 404 (Not Found)}.
         */
    @GetMapping("/farms/{id}")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<FarmDTO> getFarm(@PathVariable Long id) {
        log.debug("REST request to get Farm : {}", id);
        Optional<FarmDTO> farmDTO = farmService.findOne(id);
        return ResponseUtil.wrapOrNotFound(farmDTO);
    }

//    /**
//     * {@code DELETE  /farms/:id} : delete the "id" farm.
//     *
//     * @param id the id of the farmDTO to delete.
//     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
//     */
//    @DeleteMapping("/farms/{id}")
//    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
//    public ResponseEntity<Void> deleteFarm(@PathVariable Long id) {
//        log.debug("REST request to delete Farm : {}", id);
//        farmService.delete(id);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
//    }
}
