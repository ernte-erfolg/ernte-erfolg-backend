package ernteerfolg.backend.web.rest.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TasksInAreaPayload {

	private Set<ExtendedTaskPayload> taskList = new HashSet<>();
	private Map<Long, Double> taskIdDistanceMap = new HashMap<>();
	private int maxDistance;

	public TasksInAreaPayload(int maxDistance) {
		this.maxDistance = maxDistance;
	}
	
	public void add(ExtendedTaskPayload extendedTaskPayload, Double distance) {
		this.taskList.add(extendedTaskPayload);
		this.taskIdDistanceMap.put(extendedTaskPayload.getId(), distance);
	}
	
	public Set<ExtendedTaskPayload> getTaskList() {
		return taskList;
	}

	public void setTaskList(Set<ExtendedTaskPayload> taskList) {
		this.taskList = taskList;
	}

	public Map<Long, Double> getTaskIdDistanceMap() {
		return taskIdDistanceMap;
	}

	public void setTaskIdDistanceMap(Map<Long, Double> taskIdDistanceMap) {
		this.taskIdDistanceMap = taskIdDistanceMap;
	}

	public int getMaxDistance() {
		return maxDistance;
	}

	public void setMaxDistance(int maxDistance) {
		this.maxDistance = maxDistance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + maxDistance;
		result = prime * result + ((taskIdDistanceMap == null) ? 0 : taskIdDistanceMap.hashCode());
		result = prime * result + ((taskList == null) ? 0 : taskList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TasksInAreaPayload other = (TasksInAreaPayload) obj;
		if (maxDistance != other.maxDistance)
			return false;
		if (taskIdDistanceMap == null) {
			if (other.taskIdDistanceMap != null)
				return false;
		} else if (!taskIdDistanceMap.equals(other.taskIdDistanceMap))
			return false;
		if (taskList == null) {
			if (other.taskList != null)
				return false;
		} else if (!taskList.equals(other.taskList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TasksInAreaPayload [taskList=" + taskList + ", taskIdDistanceMap=" + taskIdDistanceMap
				+ ", maxDistance=" + maxDistance + "]";
	}

}
