package ernteerfolg.backend.web.rest.task;

import java.io.Serializable;
import java.time.LocalDate;

import ernteerfolg.backend.domain.Workday;

/**
 * A DTO for the {@link Workday} entity.
 */
public class WorkdayPayload implements Serializable {

	private static final long serialVersionUID = -5352603827897192185L;

	private Long id;
	private LocalDate date;
	private int workingHours;

	public WorkdayPayload() {
		super();
	}

	public WorkdayPayload(LocalDate date, int workingHours) {
		super();
		this.date = date;
		this.workingHours = workingHours;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(int workingHours) {
		this.workingHours = workingHours;
	}

	@Override
	public String toString() {
		return "WorkdayPayload [id=" + id + ", date=" + date + ", workingHours=" + workingHours + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + workingHours;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkdayPayload other = (WorkdayPayload) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (workingHours != other.workingHours)
			return false;
		return true;
	}
}
