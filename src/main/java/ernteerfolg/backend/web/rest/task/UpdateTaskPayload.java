package ernteerfolg.backend.web.rest.task;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.Task} entity.
 */
public class UpdateTaskPayload extends CreateTaskPayload {

	private static final long serialVersionUID = 1240218952866550266L;
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateTaskPayload other = (UpdateTaskPayload) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateTaskPayload [id=" + id + "]";
	}

}