package ernteerfolg.backend.web.rest.task;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ernteerfolg.backend.domain.Address;
import ernteerfolg.backend.domain.Farm;
import ernteerfolg.backend.domain.Helper;
import ernteerfolg.backend.domain.OfferedWorkDay;
import ernteerfolg.backend.domain.Task;
import ernteerfolg.backend.domain.User;
import ernteerfolg.backend.domain.Workday;
import ernteerfolg.backend.repository.AddressRepository;
import ernteerfolg.backend.repository.FarmRepository;
import ernteerfolg.backend.repository.TaskRepository;
import ernteerfolg.backend.service.TaskService;
import ernteerfolg.backend.service.UserService;
import ernteerfolg.backend.service.dto.TasksInAreaDTO;
import ernteerfolg.backend.service.plz.ZipGeoDataSet;
import ernteerfolg.backend.service.plz.ZipGeoService;
import ernteerfolg.backend.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * REST controller for managing {@link ernteerfolg.backend.domain.Task}.
 */
@RestController
@RequestMapping("/api")
public class TaskResource {

    private final Logger log = LoggerFactory.getLogger(TaskResource.class);

    private static final String ENTITY_NAME = "task";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TaskService taskService;

	private UserService userService;

	private ZipGeoService zipGeoService;

	private TaskRepository taskRepository;

	private FarmRepository farmRepository;

	private AddressRepository addressRepository;

    public TaskResource(TaskService taskService, UserService userService, ZipGeoService zipGeoService, TaskRepository taskRepository, FarmRepository farmRepository, AddressRepository addressRepository) {
        this.taskService = taskService;
		this.userService = userService;
		this.zipGeoService = zipGeoService;
		this.taskRepository = taskRepository;
		this.farmRepository = farmRepository;
		this.addressRepository = addressRepository;
    }

    @GetMapping("/searchTasks")
    @ApiOperation(value="", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<List<TasksInAreaPayload>> searchTasks(@RequestParam(required = true) String zip) { 

    	Optional<ZipGeoDataSet> zipDataSetOptional = zipGeoService.findByZip(zip);
    	
    	ZipGeoDataSet zipDataSet = zipDataSetOptional.orElseThrow(() -> new BadRequestAlertException("Unable to determinate coordinates for zip " + zip, ENTITY_NAME, "unprocessableZip"));
    	
    	Set<TasksInAreaPayload> tasksInAreaPayloadSet = new HashSet<>();
    	try {
			List<TasksInAreaDTO> tasksInAreasList = taskService.searchTasks(zipDataSet.getLatitude().doubleValue(), zipDataSet.getLongitude().doubleValue());
			
			List<TasksInAreaPayload> result = new ArrayList<>();
			
			for (TasksInAreaDTO tasksInArea : tasksInAreasList) {
				
				TasksInAreaPayload tasksInAreaPayload = new TasksInAreaPayload(tasksInArea.getMaxDistance());
				result.add(tasksInAreaPayload);
				tasksInAreaPayloadSet.add(tasksInAreaPayload);
				for (Task task : tasksInArea.getTaskList()) {
					tasksInAreaPayload.add( toExtendedTaskPayload(task), tasksInArea.getTaskIdDistanceMap().get(task.getId()) );
				}
			}
			
			return ResponseEntity.ok().body(result);
			
			} catch (TransformException e) {			
				throw new BadRequestAlertException("Invalid lat or lon", ENTITY_NAME, "transformException");
			}
    }
    
    /**
     * {@code POST  /tasks} : Create a new task.
     *
     * @param createTaskPayload the taskDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new taskDTO, or with status {@code 400 (Bad Request)} if the task has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tasks")
    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<ExtendedTaskPayload> createTask(@RequestBody CreateTaskPayload createTaskPayload) throws URISyntaxException {
    	
        log.debug("REST request to save Task : {}", createTaskPayload);

        ExtendedTaskPayload result;
		if(createTaskPayload.getFarmId() == null) {
			throw new BadRequestAlertException("Creating a task without farmId is not possible.", ENTITY_NAME, "farmIdMandatory");
		}
		
		Address address = new Address();
		address.setCity(createTaskPayload.getCity());
		address.setHouseNr(createTaskPayload.getHouseNr());
		address.setStreet(createTaskPayload.getStreet());
		address.setZip(createTaskPayload.getZip());
		
		addressRepository.saveAndFlush(address);
		
		Task task = new Task();
		task.setAddress(address);
		task.setDescription(createTaskPayload.getDescription());
		task.setDescriptionPlace(createTaskPayload.getAdditionalInformation());
		task.setFarm(getFarm(createTaskPayload.getFarmId()));
		task.setTitle(createTaskPayload.getTitle());
		addGeoCoorinates(createTaskPayload.getZip(), task);
		
		Set<Workday> workdaySet = createTaskPayload.getRequestedDays().stream().map(this::toWorkday).collect(Collectors.toSet());
		for (Workday workday : workdaySet) {
			workday.addTask(task);
		}
		task.setHelpers(workdaySet);
		
		result = toExtendedTaskPayload(taskRepository.save(task));
		
		return ResponseEntity.created(new URI("/api/tasks/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * Determines the Farm for the given id. 
     * @param farmId id of the farm to find.
     * @throws BadRequestAlertException if there is no farm with that id.
     * @return the farm
     */
	private Farm getFarm(Long farmId) {
		if(farmId == null) {throw new BadRequestAlertException( "Invalid farmId", "Farm", "farmIdNull");}
		Optional<Farm> farmOptional = farmRepository.findById(farmId);
        Farm farm = farmOptional.orElseThrow(() -> new BadRequestAlertException( "Invalid farmId", "Farm", "notExists"));
		return farm;
	}

	private void addGeoCoorinates(String zip, Task task) {
		Optional<ZipGeoDataSet> zipDataSetOptional = zipGeoService.findByZip(zip);
        if( zipDataSetOptional.isPresent() ) {
        	ZipGeoDataSet zipDataSet = zipDataSetOptional.get();
        	task.setLat(zipDataSet.getLatitude());
    		task.setLon(zipDataSet.getLongitude());
        }
	}

    /**
     * {@code PUT  /tasks} : Updates an existing task.
     *
     * @param updateTaskPayload the taskDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated taskDTO,
     * or with status {@code 400 (Bad Request)} if the taskDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the taskDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tasks")
    @ApiOperation(value="", authorizations = { @Authorization(value="Bearer") } )
    public ResponseEntity<ExtendedTaskPayload> updateTask(@RequestBody UpdateTaskPayload updateTaskPayload) throws URISyntaxException {
        log.debug("REST request to update Task : {}", updateTaskPayload);
        if (updateTaskPayload.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if( !taskRepository.existsById(updateTaskPayload.getId()) ) {
        	throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnotExists");
        }

        Address address = new Address();
        address.setCity(updateTaskPayload.getCity());
        address.setHouseNr(updateTaskPayload.getHouseNr());
        address.setStreet(updateTaskPayload.getStreet());
        address.setZip(updateTaskPayload.getZip());
        address.setId(updateTaskPayload.getId());
        address = addressRepository.save(address);

        Task task = new Task();
        task.setId(updateTaskPayload.getId());
        task.setAddress(address);
        task.setDescription(updateTaskPayload.getDescription());
        task.setDescriptionPlace(updateTaskPayload.getAdditionalInformation());
        task.setFarm(getFarm(updateTaskPayload.getFarmId()));
        task.setHelpers(updateTaskPayload.getRequestedDays().stream().map(this::toWorkday).collect(Collectors.toSet()));
        task.setTitle(updateTaskPayload.getTitle());
        addGeoCoorinates(updateTaskPayload.getZip(), task);
        
        ExtendedTaskPayload result = toExtendedTaskPayload(taskRepository.save(task));

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, updateTaskPayload.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tasks} : get all the tasks.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tasks in body.
     */
    @GetMapping("/tasksByFarm")
    public ResponseEntity<Set<ExtendedTaskPayload>> getAllTasksByFarm(@RequestParam(required = true) Long farmId) {

    	log.debug("REST request to get a page of Tasks");
    	// TODO: Ich hab erst einmal getAllTasks kopiert und filtere das Ergebnis. Wir müssen uns hier über das domainmodel noch klar werden.
    	// Und dann die Suche in das sql verlagern!
    	List<Task> tasks = taskRepository.findAllWithEagerRelationships();

        Set<Task> filtered = tasks.stream().filter(task -> {
	        	boolean result = false;
	        	if( Optional.ofNullable(task.getFarm()).isPresent()) {
	        		result = farmId.equals( task.getFarm().getId());
	        	}
	        	return result;        	
        	} 
        ).collect(Collectors.toSet());
        
        Set<ExtendedTaskPayload> result = new HashSet<>(); 
        for (Task task : filtered) {
        	result.add(toExtendedTaskPayload(task));
		}
        
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /tasks/:id} : get the "id" task.
     *
     * @param id the id of the taskDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the taskDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tasks/{id}")
    public ResponseEntity<ExtendedTaskPayload> getTask(@PathVariable Long id) {
        log.debug("REST request to get Task : {}", id);
        
        Optional<ExtendedTaskPayload> extendedTaskPayloadOptional = Optional.empty();
    	if( taskRepository.existsById(id) ) {
    		Task task = taskRepository.findOneWithEagerRelationships(id).orElseThrow(() ->  new RuntimeException());
			extendedTaskPayloadOptional = Optional.ofNullable(toExtendedTaskPayload(task));
    	}
       	return ResponseUtil.wrapOrNotFound(extendedTaskPayloadOptional);
    }

	private Set<WorkdayPayload> toWorkdayPayloadSet( Set<Workday> workdaySet) {
		return workdaySet.stream().map(this::toWorkdayPayload).collect(Collectors.toSet());
	}	
	
	private WorkdayPayload toWorkdayPayload( Workday serviceWorkday) {
		WorkdayPayload result = new WorkdayPayload(serviceWorkday.getDate(), Math.round(serviceWorkday.getDurationPerDay()));
		result.setId(serviceWorkday.getId());
		return result;
	}
	
	private ExtendedTaskPayload toExtendedTaskPayload(Task task) {

    	ExtendedTaskPayload extendedTaskPayload = new ExtendedTaskPayload();

		extendedTaskPayload.setId(task.getId());
		if(task.getFarm() != null) { extendedTaskPayload.setFarmId(task.getFarm().getId()); }
    	extendedTaskPayload.setTitle(task.getTitle());
    	extendedTaskPayload.setDescription(task.getDescription());
    	extendedTaskPayload.setAdditionalInformation(task.getDescriptionPlace());
    	extendedTaskPayload.setGuaranteedDays(getGuaranteedDays(task));
    	extendedTaskPayload.setHelpers(getAllHelpers(task));
    	extendedTaskPayload.setGuaranteedDaysByHelper(getGuaranteedDaysByHelper(task));
    	extendedTaskPayload.setRequestedDays(toWorkdayPayloadSet(task.getHelpers()));
    	extendedTaskPayload.setLon(task.getLon());
    	extendedTaskPayload.setLat(task.getLat());

    	Address address = task.getAddress();
		if(address != null) {
	    	extendedTaskPayload.setStreet(address.getStreet());
	    	extendedTaskPayload.setHouseNr(address.getHouseNr());
	    	extendedTaskPayload.setZip(address.getZip());
	    	extendedTaskPayload.setCity(address.getCity());
		}

        if( !isFarmer() ) {
        	extendedTaskPayload.setHelpers(null);
        	extendedTaskPayload.setGuaranteedDaysByHelper(null);
        }

		return extendedTaskPayload;
	}

	private Set<WorkdayPayload> getGuaranteedDays(Task task) {
		Set<WorkdayPayload> guaranteedDays = new HashSet<>();
    	for (Workday workday : task.getHelpers()) {
    		Float guaranteedWorkingHoursPerDay = workday.getOfferedWorkDays().stream().map(OfferedWorkDay::getDuration).reduce(new Float(0), Float::sum);
    		guaranteedDays.add(new WorkdayPayload(workday.getDate(), Math.round(guaranteedWorkingHoursPerDay)));
		}
		return guaranteedDays;
	}

	private Set<HelperPayload> getAllHelpers(Task task) {
		Set<HelperPayload> allHelpers = null;
		for (Workday workday : task.getHelpers()) {
    		allHelpers = workday.getOfferedWorkDays().stream().map(OfferedWorkDay::getHelper).map(this::toHelperPayload).collect(Collectors.toSet());
    	}
		return allHelpers;
	}
	private Map<Long, Set<WorkdayPayload>> getGuaranteedDaysByHelper(Task task) {
		Map<Long, Set<WorkdayPayload>> guaranteedDaysByHelper = new HashMap<Long, Set<WorkdayPayload>>();
    	
    	for (Workday workday : task.getHelpers()) {

    		for (OfferedWorkDay offeredWorkday : workday.getOfferedWorkDays()) {
    			Set<WorkdayPayload> workdayPayloadSetOfHelper = getWorkdayPayloadSetOfHelper(guaranteedDaysByHelper, offeredWorkday.getHelper().getId());
    			workdayPayloadSetOfHelper.add(new WorkdayPayload(workday.getDate(), Math.round(offeredWorkday.getDuration())));
			}
    		
    	}
		return guaranteedDaysByHelper;
	}

	/**
	 * Gets the WorkdayPayloadSet for the given Helper out of the passed Map. If the set ist null, it's initialized.
	 * @param guaranteedDaysByHelper
	 * @param helperId
	 * @return Gets the WorkdayPayloadSet for the given Helper 
	 */
	private Set<WorkdayPayload> getWorkdayPayloadSetOfHelper(Map<Long, Set<WorkdayPayload>> guaranteedDaysByHelper,
			Long helperId) {
		Set<WorkdayPayload> workdayPayloadSetOfHelper;
		Optional<Set<WorkdayPayload>> workdaysOfHelperOptional = Optional.ofNullable(guaranteedDaysByHelper.get(helperId));
		
		if(!workdaysOfHelperOptional.isPresent()) {
			workdayPayloadSetOfHelper = new HashSet<WorkdayPayload>();
			guaranteedDaysByHelper.put(helperId, workdayPayloadSetOfHelper);
		} else {
			workdayPayloadSetOfHelper = workdaysOfHelperOptional.get();
		}
		return workdayPayloadSetOfHelper;
	}

	private HelperPayload toHelperPayload(Helper helper) {
		HelperPayload helperDTO = new HelperPayload();
		helperDTO.setId(helper.getId());
		helperDTO.setExperience(helper.getExperience());
		helperDTO.setFirstName(helper.getUserExtend().getUser().getFirstName());
		helperDTO.setLastName(helper.getUserExtend().getUser().getLastName());
		helperDTO.setLat(helper.getUserExtend().getAddress().getLat());
		helperDTO.setLon(helper.getUserExtend().getAddress().getLon());
		helperDTO.setStreet(helper.getUserExtend().getAddress().getStreet());
		helperDTO.setHouseNr(helper.getUserExtend().getAddress().getHouseNr());
		helperDTO.setZipCode(helper.getUserExtend().getAddress().getZip());
		helperDTO.setCity(helper.getUserExtend().getAddress().getCity());
		helperDTO.setPhone(helper.getUserExtend().getMobilePhone());
		helperDTO.seteMail(helper.getUserExtend().getUser().getEmail());
		return helperDTO;
	}

	private Workday toWorkday(WorkdayPayload workdayPayload) {
		Workday result = new Workday();
		result.setDate(workdayPayload.getDate());
		result.setDurationPerDay(new Float(workdayPayload.getWorkingHours()));
		return result;
	}

	private boolean isFarmer() {
		String farmerRoleName = "ROLE_ADMIN";
        Optional<User> userOptional = userService.getUserWithAuthorities();
        if(!userOptional.isPresent()) {
        	throw new BadRequestAlertException("Invalid request", ENTITY_NAME, "missingAuthenticatedUser");
        }
        User user = userOptional.get();

        boolean isFarmer = user.getAuthorities().stream().filter(autority -> farmerRoleName.equals(autority.getName())).count() > 0;
        return isFarmer;
	}
}
