package ernteerfolg.backend.web.rest.task;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.Task} entity.
 */
public class CreateTaskPayload implements Serializable {

	private static final long serialVersionUID = -1885418989476641538L;
	private Long farmId;
	private String title;
	private String description;
	private Set<WorkdayPayload> requestedDays = new HashSet<>();
	private String street;
	private String houseNr;
	private String zip;
	private String city;
	/**
	 * This attribute is for additional information about the place, so the helpers
	 * can find it more easily (e. g. directions to the field)
	 */
	private String additionalInformation;

	public Long getFarmId() {
		return farmId;
	}

	public void setFarmId(Long farmId) {
		this.farmId = farmId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<WorkdayPayload> getRequestedDays() {
		return requestedDays;
	}

	public void setRequestedDays(Set<WorkdayPayload> requestedDays) {
		this.requestedDays = requestedDays;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNr() {
		return houseNr;
	}

	public void setHouseNr(String houseNr) {
		this.houseNr = houseNr;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalInformation == null) ? 0 : additionalInformation.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((farmId == null) ? 0 : farmId.hashCode());
		result = prime * result + ((houseNr == null) ? 0 : houseNr.hashCode());
		result = prime * result + ((requestedDays == null) ? 0 : requestedDays.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreateTaskPayload other = (CreateTaskPayload) obj;
		if (additionalInformation == null) {
			if (other.additionalInformation != null)
				return false;
		} else if (!additionalInformation.equals(other.additionalInformation))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (farmId == null) {
			if (other.farmId != null)
				return false;
		} else if (!farmId.equals(other.farmId))
			return false;
		if (houseNr == null) {
			if (other.houseNr != null)
				return false;
		} else if (!houseNr.equals(other.houseNr))
			return false;
		if (requestedDays == null) {
			if (other.requestedDays != null)
				return false;
		} else if (!requestedDays.equals(other.requestedDays))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CreateTaskPayload [farmId=" + farmId + ", title=" + title + ", description=" + description
				+ ", requestedDays=" + requestedDays + ", street=" + street
				+ ", houseNr=" + houseNr + ", zip=" + zip + ", city=" + city + ", additionalInformation="
				+ additionalInformation + "]";
	}

}