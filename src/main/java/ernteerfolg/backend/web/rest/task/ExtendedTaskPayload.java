package ernteerfolg.backend.web.rest.task;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A DTO for the {@link ernteerfolg.backend.domain.Task} entity.
 */
public class ExtendedTaskPayload extends UpdateTaskPayload {

	private static final long serialVersionUID = -9186453149761106366L;
	private BigDecimal lon;
	private BigDecimal lat;
	private Set<WorkdayPayload> guaranteedDays = new HashSet<>();
	private Set<HelperPayload> helpers = new HashSet<>();
	private Map<Long, Set<WorkdayPayload>> guaranteedDaysByHelper = new HashMap<Long, Set<WorkdayPayload>>();

	public Set<WorkdayPayload> getGuaranteedDays() {
		return guaranteedDays;
	}

	public void setGuaranteedDays(Set<WorkdayPayload> guaranteedDays) {
		this.guaranteedDays = guaranteedDays;
	}

	public Set<HelperPayload> getHelpers() {
		return helpers;
	}

	public void setHelpers(Set<HelperPayload> helpers) {
		this.helpers = helpers;
	}

	public Map<Long, Set<WorkdayPayload>> getGuaranteedDaysByHelper() {
		return guaranteedDaysByHelper;
	}

	public void setGuaranteedDaysByHelper(Map<Long, Set<WorkdayPayload>> guaranteedDaysByHelper) {
		this.guaranteedDaysByHelper = guaranteedDaysByHelper;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getLon() {
		return lon;
	}

	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((guaranteedDays == null) ? 0 : guaranteedDays.hashCode());
		result = prime * result + ((guaranteedDaysByHelper == null) ? 0 : guaranteedDaysByHelper.hashCode());
		result = prime * result + ((helpers == null) ? 0 : helpers.hashCode());
		result = prime * result + ((lat == null) ? 0 : lat.hashCode());
		result = prime * result + ((lon == null) ? 0 : lon.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtendedTaskPayload other = (ExtendedTaskPayload) obj;
		if (guaranteedDays == null) {
			if (other.guaranteedDays != null)
				return false;
		} else if (!guaranteedDays.equals(other.guaranteedDays))
			return false;
		if (guaranteedDaysByHelper == null) {
			if (other.guaranteedDaysByHelper != null)
				return false;
		} else if (!guaranteedDaysByHelper.equals(other.guaranteedDaysByHelper))
			return false;
		if (helpers == null) {
			if (other.helpers != null)
				return false;
		} else if (!helpers.equals(other.helpers))
			return false;
		if (lat == null) {
			if (other.lat != null)
				return false;
		} else if (!lat.equals(other.lat))
			return false;
		if (lon == null) {
			if (other.lon != null)
				return false;
		} else if (!lon.equals(other.lon))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExtendedTaskPayload [lon=" + lon + ", lat=" + lat + ", guaranteedDays=" + guaranteedDays + ", helpers="
				+ helpers + ", guaranteedDaysByHelper=" + guaranteedDaysByHelper + "]";
	}
}
