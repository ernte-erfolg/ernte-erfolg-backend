package ernteerfolg.backend.repository;

import ernteerfolg.backend.domain.Task;
import ernteerfolg.backend.domain.Workday;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Spring Data  repository for the Date entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorkDayRepository extends JpaRepository<Workday, Long> {

}
