package ernteerfolg.backend.repository;

import ernteerfolg.backend.domain.Helper;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Helper entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HelperRepository extends JpaRepository<Helper, Long> {
}
