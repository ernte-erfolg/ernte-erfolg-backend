package ernteerfolg.backend.repository;

import ernteerfolg.backend.domain.Helper;
import ernteerfolg.backend.domain.OfferedWorkDay;

import ernteerfolg.backend.domain.Workday;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the HelperToDate entity.
 */
@Repository
public interface OfferedWorkDayRepository extends JpaRepository<OfferedWorkDay, Long> {

    Set<OfferedWorkDay> findAllByHelperAndWorkdayIn(Helper helper, Collection<Workday> workday);

    Set<OfferedWorkDay> findAllByWorkdayIn(Collection<Workday> workday);



}
