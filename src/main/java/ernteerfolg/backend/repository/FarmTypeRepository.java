package ernteerfolg.backend.repository;

import ernteerfolg.backend.domain.FarmType;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FarmType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FarmTypeRepository extends JpaRepository<FarmType, Long> {
}
