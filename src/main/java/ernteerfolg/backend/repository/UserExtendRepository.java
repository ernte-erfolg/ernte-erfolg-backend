package ernteerfolg.backend.repository;

import ernteerfolg.backend.domain.Helper;
import ernteerfolg.backend.domain.User;
import ernteerfolg.backend.domain.UserExtend;

import ernteerfolg.backend.service.dto.UserExtendDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the UserExtend entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserExtendRepository extends JpaRepository<UserExtend, Long> {

    Optional<UserExtend> findUserExtendByUser(User user);

    UserExtend findUserExtendByHelperId(Long helper_id);
}
