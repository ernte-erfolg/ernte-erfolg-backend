package ernteerfolg.backend.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class FarmTypeMapperTest {

    private FarmTypeMapper farmTypeMapper;

    @BeforeEach
    public void setUp() {
        farmTypeMapper = new FarmTypeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(farmTypeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(farmTypeMapper.fromId(null)).isNull();
    }
}
