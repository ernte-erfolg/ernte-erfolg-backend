package ernteerfolg.backend.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class WorkdayMapperTest {

    private WorkdayMapper workdayMapper;

    @BeforeEach
    public void setUp() {
        workdayMapper = new WorkdayMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(workdayMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(workdayMapper.fromId(null)).isNull();
    }
}
