package ernteerfolg.backend.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OfferedWorkDayMapperTest {

    private OfferedWorkDayMapper offeredWorkDayMapper;

    @BeforeEach
    public void setUp() {
        offeredWorkDayMapper = new OfferedWorkDayMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(offeredWorkDayMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(offeredWorkDayMapper.fromId(null)).isNull();
    }
}
