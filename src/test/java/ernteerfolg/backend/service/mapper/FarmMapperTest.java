package ernteerfolg.backend.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class FarmMapperTest {

    private FarmMapper farmMapper;

    @BeforeEach
    public void setUp() {
        farmMapper = new FarmMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(farmMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(farmMapper.fromId(null)).isNull();
    }
}
