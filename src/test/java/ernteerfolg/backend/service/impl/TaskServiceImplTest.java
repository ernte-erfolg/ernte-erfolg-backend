package ernteerfolg.backend.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.opengis.referencing.operation.TransformException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import ernteerfolg.backend.domain.Task;
import ernteerfolg.backend.repository.TaskRepository;
import ernteerfolg.backend.service.dto.TasksInAreaDTO;
class TaskServiceImplTest implements TaskRepository {

	private List<Task> tasks;
	
	@Test
	void testTaskWithWronZip() {
				
		this.tasks = new ArrayList<>();
		this.tasks.add( new Task() );
		
		TaskServiceImpl taskService = new TaskServiceImpl(this);
		int[][] areas = {{0,1},{2,20},{21,30}};
		taskService.setSearchDistanceAreas(areas);
		
		List<TasksInAreaDTO> foundTasks = null;
		try {
			foundTasks = taskService.searchTasks(48.13137641089567, 11.678686072900279);
		} catch (TransformException e) {
			fail(e);
		}
	}

	
	@Test
	void onlyFirstArea() {
		
		this.tasks = new ArrayList<>();
		this.tasks.add( createTask(48.13138212235482, 11.687564849853516, "Messestadt") );
		this.tasks.add( createTask(48.14535788899587, 11.734085083007814, "Feldkirchen") );
		this.tasks.add( createTask(48.18577453931438, 11.85527801513672, "Markt Schwaben" ) );
		
		TaskServiceImpl taskService = new TaskServiceImpl(this);
		
		List<TasksInAreaDTO> foundTasks = null;
		try {
			foundTasks = taskService.searchTasks(48.13137641089567, 11.678686072900279);
		} catch (TransformException e) {
			fail(e);
		}
		assertThat(foundTasks ).isNotNull();
		assertThat(foundTasks.size() ).isEqualTo(1);
		assertThat(foundTasks.get(0).getMaxDistance()).isEqualTo(30);
	}

	@Test
	void onlySecondArea() {
		
		this.tasks = new ArrayList<>();
		this.tasks.add( createTask(48.14535788899587, 11.734085083007814, "Feldkirchen") );
		this.tasks.add( createTask(48.18577453931438, 11.85527801513672, "Markt Schwaben" ) );
		
		TaskServiceImpl taskService = new TaskServiceImpl(this);
		int[][] areas = {{0,1},{2,20},{21,30}};
		taskService.setSearchDistanceAreas(areas);
		
		List<TasksInAreaDTO> foundTasks = null;
		try {
			foundTasks = taskService.searchTasks(48.13137641089567, 11.678686072900279);
		} catch (TransformException e) {
			fail(e);
		}
		assertThat(foundTasks).isNotNull();
		assertThat(foundTasks.size() ).isEqualTo(1);	
		assertThat(foundTasks.get(0).getMaxDistance()).isEqualTo(20);
	}

	@Test
	void secondAndThirdArea() {
		
		this.tasks = new ArrayList<>();
		this.tasks.add( createTask(48.14535788899587, 11.734085083007814, "Feldkirchen") );
		this.tasks.add( createTask(48.18577453931438, 11.85527801513672, "Markt Schwaben" ) );
		
		TaskServiceImpl taskService = new TaskServiceImpl(this);
		int[][] areas = {{0,1},{2,7},{8,22}};
		taskService.setSearchDistanceAreas(areas);
		
		List<TasksInAreaDTO> foundTasks = null;
		try {
			foundTasks = taskService.searchTasks(48.13137641089567, 11.678686072900279);
		} catch (TransformException e) {
			fail(e);
		}
		assertThat(foundTasks ).isNotNull();
		assertThat(foundTasks.size() ).isEqualTo(2);

		assertThat(foundTasks.get(0).getMaxDistance()).isEqualTo(7);
		assertThat(foundTasks.get(1).getMaxDistance()).isEqualTo(22);
	}

	@Test
	void secondAndFourthArea() {
		
		this.tasks = new ArrayList<>();
		this.tasks.add( createTask(48.14535788899587, 11.734085083007814, "Feldkirchen") );
		this.tasks.add( createTask(48.18577453931438, 11.85527801513672, "Markt Schwaben" ) );
		
		TaskServiceImpl taskService = new TaskServiceImpl(this);
		int[][] areas = {{0,1},{2,7},{8,9},{10,22}};
		taskService.setSearchDistanceAreas(areas);
		
		List<TasksInAreaDTO> foundTasks = null;
		try {
			foundTasks = taskService.searchTasks(48.13137641089567, 11.678686072900279);
		} catch (TransformException e) {
			fail(e);
		}
		assertThat(foundTasks ).isNotNull();
		assertThat(foundTasks.size() ).isEqualTo(2);		
		
		assertThat(foundTasks.get(0).getMaxDistance()).isEqualTo(7);
		assertThat(foundTasks.get(1).getMaxDistance()).isEqualTo(22);
		
		for (Entry<Long,Double> taskIdDistanceMapEntry : foundTasks.get(0).getTaskIdDistanceMap().entrySet()) {
			assertThat(taskIdDistanceMapEntry.getValue() < 7  ).isTrue();
		}
		for (Entry<Long,Double> taskIdDistanceMapEntry : foundTasks.get(1).getTaskIdDistanceMap().entrySet()) {
			assertThat(taskIdDistanceMapEntry.getValue() > 10  ).isTrue();
		}
	}

	private Task createTask(double lat, double lon, String title) {
		Task t1 = new Task();
		t1.setLat(new BigDecimal(lat));
		t1.setLon(new BigDecimal(lon));
		t1.setTitle(title);
		return t1;
	}

	@Override
	public List<Task> findAll() {
		return tasks;
	}

	@Override
	public List<Task> findAll(Sort sort) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Task> findAllById(Iterable<Long> ids) {
		throw new UnsupportedOperationException();	
	}

	@Override
	public <S extends Task> List<S> saveAll(Iterable<S> entities) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void flush() {
		throw new UnsupportedOperationException();		
	}

	@Override
	public <S extends Task> S saveAndFlush(S entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteInBatch(Iterable<Task> entities) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteAllInBatch() {
		throw new UnsupportedOperationException();		
	}

	@Override
	public Task getOne(Long id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <S extends Task> List<S> findAll(Example<S> example) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <S extends Task> List<S> findAll(Example<S> example, Sort sort) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Page<Task> findAll(Pageable pageable) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <S extends Task> S save(S entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Optional<Task> findById(Long id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean existsById(Long id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public long count() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteById(Long id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(Task entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteAll(Iterable<? extends Task> entities) {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void deleteAll() {
		throw new UnsupportedOperationException();	
	}

	@Override
	public <S extends Task> Optional<S> findOne(Example<S> example) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <S extends Task> Page<S> findAll(Example<S> example, Pageable pageable) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <S extends Task> long count(Example<S> example) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <S extends Task> boolean exists(Example<S> example) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Page<Task> findAllWithEagerRelationships(Pageable pageable) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Task> findAllWithEagerRelationships() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Optional<Task> findOneWithEagerRelationships(Long id) {
		throw new UnsupportedOperationException();
	}
}
