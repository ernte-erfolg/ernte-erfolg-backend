package ernteerfolg.backend.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

public class WorkdayDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(WorkdayDTO.class);
        WorkdayDTO workdayDTO1 = new WorkdayDTO();
        workdayDTO1.setId(1L);
        WorkdayDTO workdayDTO2 = new WorkdayDTO();
        assertThat(workdayDTO1).isNotEqualTo(workdayDTO2);
        workdayDTO2.setId(workdayDTO1.getId());
        assertThat(workdayDTO1).isEqualTo(workdayDTO2);
        workdayDTO2.setId(2L);
        assertThat(workdayDTO1).isNotEqualTo(workdayDTO2);
        workdayDTO1.setId(null);
        assertThat(workdayDTO1).isNotEqualTo(workdayDTO2);
    }
}
