package ernteerfolg.backend.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

public class OfferedWorkDayDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OfferedWorkDayDTO.class);
        OfferedWorkDayDTO offeredWorkDayDTO1 = new OfferedWorkDayDTO();
        offeredWorkDayDTO1.setId(1L);
        OfferedWorkDayDTO offeredWorkDayDTO2 = new OfferedWorkDayDTO();
        assertThat(offeredWorkDayDTO1).isNotEqualTo(offeredWorkDayDTO2);
        offeredWorkDayDTO2.setId(offeredWorkDayDTO1.getId());
        assertThat(offeredWorkDayDTO1).isEqualTo(offeredWorkDayDTO2);
        offeredWorkDayDTO2.setId(2L);
        assertThat(offeredWorkDayDTO1).isNotEqualTo(offeredWorkDayDTO2);
        offeredWorkDayDTO1.setId(null);
        assertThat(offeredWorkDayDTO1).isNotEqualTo(offeredWorkDayDTO2);
    }
}
