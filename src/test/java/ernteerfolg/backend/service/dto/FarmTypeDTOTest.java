package ernteerfolg.backend.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

public class FarmTypeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FarmTypeDTO.class);
        FarmTypeDTO farmTypeDTO1 = new FarmTypeDTO();
        farmTypeDTO1.setId(1L);
        FarmTypeDTO farmTypeDTO2 = new FarmTypeDTO();
        assertThat(farmTypeDTO1).isNotEqualTo(farmTypeDTO2);
        farmTypeDTO2.setId(farmTypeDTO1.getId());
        assertThat(farmTypeDTO1).isEqualTo(farmTypeDTO2);
        farmTypeDTO2.setId(2L);
        assertThat(farmTypeDTO1).isNotEqualTo(farmTypeDTO2);
        farmTypeDTO1.setId(null);
        assertThat(farmTypeDTO1).isNotEqualTo(farmTypeDTO2);
    }
}
