package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.ErnteErfolgBackendApp;
import ernteerfolg.backend.domain.Workday;
import ernteerfolg.backend.repository.WorkDayRepository;
import ernteerfolg.backend.service.DateService;
import ernteerfolg.backend.service.dto.WorkdayDTO;
import ernteerfolg.backend.service.mapper.WorkdayMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DateResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgBackendApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class WorkdayResourceIT {

    private static final Integer DEFAULT_DAY = 1;
    private static final Integer UPDATED_DAY = 2;

    private static final Integer DEFAULT_MONTH = 1;
    private static final Integer UPDATED_MONTH = 2;

    private static final Integer DEFAULT_YEAR = 1;
    private static final Integer UPDATED_YEAR = 2;

    private static final Float DEFAULT_DURATION_PER_DAY = 1F;
    private static final Float UPDATED_DURATION_PER_DAY = 2F;

    @Autowired
    private WorkDayRepository workDayRepository;

    @Autowired
    private WorkdayMapper workdayMapper;

    @Autowired
    private DateService dateService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDateMockMvc;

    private Workday workday;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workday createEntity(EntityManager em) {
        Workday workday = new Workday()
            .date(LocalDate.of(DEFAULT_YEAR,DEFAULT_MONTH,DEFAULT_DAY))
            .durationPerDay(DEFAULT_DURATION_PER_DAY);
        return workday;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workday createUpdatedEntity(EntityManager em) {
        Workday workday = new Workday()
            .date(LocalDate.of(UPDATED_YEAR,UPDATED_MONTH,UPDATED_DAY))
            .durationPerDay(UPDATED_DURATION_PER_DAY);
        return workday;
    }

    @BeforeEach
    public void initTest() {
        workday = createEntity(em);
    }

//    @Test
 //   @Transactional
    public void createDate() throws Exception {
        int databaseSizeBeforeCreate = workDayRepository.findAll().size();

        // Create the Date
        WorkdayDTO workdayDTO = workdayMapper.toDto(workday);
        restDateMockMvc.perform(post("/api/dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workdayDTO)))
            .andExpect(status().isCreated());

        // Validate the Date in the database
        List<Workday> workdayList = workDayRepository.findAll();
        assertThat(workdayList).hasSize(databaseSizeBeforeCreate + 1);
        Workday testWorkday = workdayList.get(workdayList.size() - 1);
        assertThat(testWorkday.getDurationPerDay()).isEqualTo(DEFAULT_DURATION_PER_DAY);
    }

   // @Test
  //  @Transactional
    public void createDateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = workDayRepository.findAll().size();

        // Create the Date with an existing ID
        workday.setId(1L);
        WorkdayDTO workdayDTO = workdayMapper.toDto(workday);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDateMockMvc.perform(post("/api/dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workdayDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Date in the database
        List<Workday> workdayList = workDayRepository.findAll();
        assertThat(workdayList).hasSize(databaseSizeBeforeCreate);
    }


 //   @Test
 //   @Transactional
    public void getAllDates() throws Exception {
        // Initialize the database
        workDayRepository.saveAndFlush(workday);

        // Get all the dateList
        restDateMockMvc.perform(get("/api/dates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(workday.getId().intValue())))
            .andExpect(jsonPath("$.date").value(LocalDate.of(DEFAULT_YEAR,DEFAULT_MONTH,DEFAULT_DAY)))
            .andExpect(jsonPath("$.[*].durationPerDay").value(hasItem(DEFAULT_DURATION_PER_DAY.doubleValue())));
    }

  //  @Test
  //  @Transactional
    public void getDate() throws Exception {
        // Initialize the database
        workDayRepository.saveAndFlush(workday);

        // Get the date
        restDateMockMvc.perform(get("/api/dates/{id}", workday.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(workday.getId().intValue()))
            .andExpect(jsonPath("$.date").value(LocalDate.of(DEFAULT_YEAR,DEFAULT_MONTH,DEFAULT_DAY)))
            .andExpect(jsonPath("$.durationPerDay").value(DEFAULT_DURATION_PER_DAY.doubleValue()));
    }

//    @Test
//    @Transactional
    public void getNonExistingDate() throws Exception {
        // Get the date
        restDateMockMvc.perform(get("/api/dates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

//    @Test
//    @Transactional
    public void updateDate() throws Exception {
        // Initialize the database
        workDayRepository.saveAndFlush(workday);

        int databaseSizeBeforeUpdate = workDayRepository.findAll().size();

        // Update the date
        Workday updatedWorkday = workDayRepository.findById(workday.getId()).get();
        // Disconnect from session so that the updates on updatedDate are not directly saved in db
        em.detach(updatedWorkday);
        updatedWorkday
            .date(LocalDate.of(UPDATED_YEAR,UPDATED_MONTH,UPDATED_DAY))
            .durationPerDay(UPDATED_DURATION_PER_DAY);
        WorkdayDTO workdayDTO = workdayMapper.toDto(updatedWorkday);

        restDateMockMvc.perform(put("/api/dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workdayDTO)))
            .andExpect(status().isOk());

        // Validate the Date in the database
        List<Workday> workdayList = workDayRepository.findAll();
        assertThat(workdayList).hasSize(databaseSizeBeforeUpdate);
        Workday testWorkday = workdayList.get(workdayList.size() - 1);
        assertThat(testWorkday.getDurationPerDay()).isEqualTo(UPDATED_DURATION_PER_DAY);
    }

//    @Test
//    @Transactional
    public void updateNonExistingDate() throws Exception {
        int databaseSizeBeforeUpdate = workDayRepository.findAll().size();

        // Create the Date
        WorkdayDTO workdayDTO = workdayMapper.toDto(workday);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDateMockMvc.perform(put("/api/dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workdayDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Date in the database
        List<Workday> workdayList = workDayRepository.findAll();
        assertThat(workdayList).hasSize(databaseSizeBeforeUpdate);
    }

//    @Test
//    @Transactional
    public void deleteDate() throws Exception {
        // Initialize the database
        workDayRepository.saveAndFlush(workday);

        int databaseSizeBeforeDelete = workDayRepository.findAll().size();

        // Delete the date
        restDateMockMvc.perform(delete("/api/dates/{id}", workday.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Workday> workdayList = workDayRepository.findAll();
        assertThat(workdayList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
