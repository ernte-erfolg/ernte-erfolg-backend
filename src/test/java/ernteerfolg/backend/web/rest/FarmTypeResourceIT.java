package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.ErnteErfolgBackendApp;
import ernteerfolg.backend.domain.FarmType;
import ernteerfolg.backend.repository.FarmTypeRepository;
import ernteerfolg.backend.service.FarmTypeService;
import ernteerfolg.backend.service.dto.FarmTypeDTO;
import ernteerfolg.backend.service.mapper.FarmTypeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FarmTypeResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgBackendApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class FarmTypeResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private FarmTypeRepository farmTypeRepository;

    @Autowired
    private FarmTypeMapper farmTypeMapper;

    @Autowired
    private FarmTypeService farmTypeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFarmTypeMockMvc;

    private FarmType farmType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FarmType createEntity(EntityManager em) {
        FarmType farmType = new FarmType()
            .description(DEFAULT_DESCRIPTION);
        return farmType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FarmType createUpdatedEntity(EntityManager em) {
        FarmType farmType = new FarmType()
            .description(UPDATED_DESCRIPTION);
        return farmType;
    }

    @BeforeEach
    public void initTest() {
        farmType = createEntity(em);
    }

//    @Test
//    @Transactional
    public void createFarmType() throws Exception {
        int databaseSizeBeforeCreate = farmTypeRepository.findAll().size();

        // Create the FarmType
        FarmTypeDTO farmTypeDTO = farmTypeMapper.toDto(farmType);
        restFarmTypeMockMvc.perform(post("/api/farm-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(farmTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the FarmType in the database
        List<FarmType> farmTypeList = farmTypeRepository.findAll();
        assertThat(farmTypeList).hasSize(databaseSizeBeforeCreate + 1);
        FarmType testFarmType = farmTypeList.get(farmTypeList.size() - 1);
        assertThat(testFarmType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

//    @Test
//    @Transactional
    public void createFarmTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = farmTypeRepository.findAll().size();

        // Create the FarmType with an existing ID
        farmType.setId(1L);
        FarmTypeDTO farmTypeDTO = farmTypeMapper.toDto(farmType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFarmTypeMockMvc.perform(post("/api/farm-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(farmTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FarmType in the database
        List<FarmType> farmTypeList = farmTypeRepository.findAll();
        assertThat(farmTypeList).hasSize(databaseSizeBeforeCreate);
    }


//    @Test
//    @Transactional
    public void getAllFarmTypes() throws Exception {
        // Initialize the database
        farmTypeRepository.saveAndFlush(farmType);

        // Get all the farmTypeList
        restFarmTypeMockMvc.perform(get("/api/farm-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(farmType.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
//    @Test
//    @Transactional
    public void getFarmType() throws Exception {
        // Initialize the database
        farmTypeRepository.saveAndFlush(farmType);

        // Get the farmType
        restFarmTypeMockMvc.perform(get("/api/farm-types/{id}", farmType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(farmType.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

//    @Test
//    @Transactional
    public void getNonExistingFarmType() throws Exception {
        // Get the farmType
        restFarmTypeMockMvc.perform(get("/api/farm-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

//    @Test
//    @Transactional
    public void updateFarmType() throws Exception {
        // Initialize the database
        farmTypeRepository.saveAndFlush(farmType);

        int databaseSizeBeforeUpdate = farmTypeRepository.findAll().size();

        // Update the farmType
        FarmType updatedFarmType = farmTypeRepository.findById(farmType.getId()).get();
        // Disconnect from session so that the updates on updatedFarmType are not directly saved in db
        em.detach(updatedFarmType);
        updatedFarmType
            .description(UPDATED_DESCRIPTION);
        FarmTypeDTO farmTypeDTO = farmTypeMapper.toDto(updatedFarmType);

        restFarmTypeMockMvc.perform(put("/api/farm-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(farmTypeDTO)))
            .andExpect(status().isOk());

        // Validate the FarmType in the database
        List<FarmType> farmTypeList = farmTypeRepository.findAll();
        assertThat(farmTypeList).hasSize(databaseSizeBeforeUpdate);
        FarmType testFarmType = farmTypeList.get(farmTypeList.size() - 1);
        assertThat(testFarmType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

//    @Test
//    @Transactional
    public void updateNonExistingFarmType() throws Exception {
        int databaseSizeBeforeUpdate = farmTypeRepository.findAll().size();

        // Create the FarmType
        FarmTypeDTO farmTypeDTO = farmTypeMapper.toDto(farmType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFarmTypeMockMvc.perform(put("/api/farm-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(farmTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FarmType in the database
        List<FarmType> farmTypeList = farmTypeRepository.findAll();
        assertThat(farmTypeList).hasSize(databaseSizeBeforeUpdate);
    }

//    @Test
//    @Transactional
    public void deleteFarmType() throws Exception {
        // Initialize the database
        farmTypeRepository.saveAndFlush(farmType);

        int databaseSizeBeforeDelete = farmTypeRepository.findAll().size();

        // Delete the farmType
        restFarmTypeMockMvc.perform(delete("/api/farm-types/{id}", farmType.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FarmType> farmTypeList = farmTypeRepository.findAll();
        assertThat(farmTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
