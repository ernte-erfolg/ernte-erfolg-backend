package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.ErnteErfolgBackendApp;
import ernteerfolg.backend.domain.OfferedWorkDay;
import ernteerfolg.backend.repository.OfferedWorkDayRepository;
import ernteerfolg.backend.service.OfferedWorkDayService;
import ernteerfolg.backend.service.dto.OfferedWorkDayDTO;
import ernteerfolg.backend.service.mapper.OfferedWorkDayMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OfferedWorkDayResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgBackendApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class OfferedWorkDayResourceIT {

    private static final Boolean DEFAULT_SUCCESS = false;
    private static final Boolean UPDATED_SUCCESS = true;

    private static final Float DEFAULT_DURATION = 1F;
    private static final Float UPDATED_DURATION = 2F;

    @Autowired
    private OfferedWorkDayRepository offeredWorkDayRepository;

    @Mock
    private OfferedWorkDayRepository offeredWorkDayRepositoryMock;

    @Autowired
    private OfferedWorkDayMapper offeredWorkDayMapper;

    @Mock
    private OfferedWorkDayService offeredWorkDayServiceMock;

    @Autowired
    private OfferedWorkDayService offeredWorkDayService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHelperToDateMockMvc;

    private OfferedWorkDay offeredWorkDay;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OfferedWorkDay createEntity(EntityManager em) {
        OfferedWorkDay offeredWorkDay = new OfferedWorkDay()
            .success(DEFAULT_SUCCESS)
            .duration(DEFAULT_DURATION);
        return offeredWorkDay;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OfferedWorkDay createUpdatedEntity(EntityManager em) {
        OfferedWorkDay offeredWorkDay = new OfferedWorkDay()
            .success(UPDATED_SUCCESS)
            .duration(UPDATED_DURATION);
        return offeredWorkDay;
    }

    @BeforeEach
    public void initTest() {
        offeredWorkDay = createEntity(em);
    }

//    @Test
//    @Transactional
    public void createHelperToDate() throws Exception {
        int databaseSizeBeforeCreate = offeredWorkDayRepository.findAll().size();

        // Create the HelperToDate
        OfferedWorkDayDTO offeredWorkDayDTO = offeredWorkDayMapper.toDto(offeredWorkDay);
        restHelperToDateMockMvc.perform(post("/api/helper-to-dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offeredWorkDayDTO)))
            .andExpect(status().isCreated());

        // Validate the HelperToDate in the database
        List<OfferedWorkDay> offeredWorkDayList = offeredWorkDayRepository.findAll();
        assertThat(offeredWorkDayList).hasSize(databaseSizeBeforeCreate + 1);
        OfferedWorkDay testOfferedWorkDay = offeredWorkDayList.get(offeredWorkDayList.size() - 1);
        assertThat(testOfferedWorkDay.isSuccess()).isEqualTo(DEFAULT_SUCCESS);
        assertThat(testOfferedWorkDay.getDuration()).isEqualTo(DEFAULT_DURATION);
    }

//    @Test
//    @Transactional
    public void createHelperToDateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = offeredWorkDayRepository.findAll().size();

        // Create the HelperToDate with an existing ID
        offeredWorkDay.setId(1L);
        OfferedWorkDayDTO offeredWorkDayDTO = offeredWorkDayMapper.toDto(offeredWorkDay);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHelperToDateMockMvc.perform(post("/api/helper-to-dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offeredWorkDayDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HelperToDate in the database
        List<OfferedWorkDay> offeredWorkDayList = offeredWorkDayRepository.findAll();
        assertThat(offeredWorkDayList).hasSize(databaseSizeBeforeCreate);
    }


//    @Test
//    @Transactional
    public void getAllHelperToDates() throws Exception {
        // Initialize the database
        offeredWorkDayRepository.saveAndFlush(offeredWorkDay);

        // Get all the helperToDateList
        restHelperToDateMockMvc.perform(get("/api/helper-to-dates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(offeredWorkDay.getId().intValue())))
            .andExpect(jsonPath("$.[*].success").value(hasItem(DEFAULT_SUCCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.doubleValue())));
    }


//    @Test
//    @Transactional
    public void getHelperToDate() throws Exception {
        // Initialize the database
        offeredWorkDayRepository.saveAndFlush(offeredWorkDay);

        // Get the helperToDate
        restHelperToDateMockMvc.perform(get("/api/helper-to-dates/{id}", offeredWorkDay.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(offeredWorkDay.getId().intValue()))
            .andExpect(jsonPath("$.success").value(DEFAULT_SUCCESS.booleanValue()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.doubleValue()));
    }

//    @Test
//    @Transactional
    public void getNonExistingHelperToDate() throws Exception {
        // Get the helperToDate
        restHelperToDateMockMvc.perform(get("/api/helper-to-dates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

//    @Test
//    @Transactional
    public void updateHelperToDate() throws Exception {
        // Initialize the database
        offeredWorkDayRepository.saveAndFlush(offeredWorkDay);

        int databaseSizeBeforeUpdate = offeredWorkDayRepository.findAll().size();

        // Update the helperToDate
        OfferedWorkDay updatedOfferedWorkDay = offeredWorkDayRepository.findById(offeredWorkDay.getId()).get();
        // Disconnect from session so that the updates on updatedHelperToDate are not directly saved in db
        em.detach(updatedOfferedWorkDay);
        updatedOfferedWorkDay
            .success(UPDATED_SUCCESS)
            .duration(UPDATED_DURATION);
        OfferedWorkDayDTO offeredWorkDayDTO = offeredWorkDayMapper.toDto(updatedOfferedWorkDay);

        restHelperToDateMockMvc.perform(put("/api/helper-to-dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offeredWorkDayDTO)))
            .andExpect(status().isOk());

        // Validate the HelperToDate in the database
        List<OfferedWorkDay> offeredWorkDayList = offeredWorkDayRepository.findAll();
        assertThat(offeredWorkDayList).hasSize(databaseSizeBeforeUpdate);
        OfferedWorkDay testOfferedWorkDay = offeredWorkDayList.get(offeredWorkDayList.size() - 1);
        assertThat(testOfferedWorkDay.isSuccess()).isEqualTo(UPDATED_SUCCESS);
        assertThat(testOfferedWorkDay.getDuration()).isEqualTo(UPDATED_DURATION);
    }

//    @Test
//    @Transactional
    public void updateNonExistingHelperToDate() throws Exception {
        int databaseSizeBeforeUpdate = offeredWorkDayRepository.findAll().size();

        // Create the HelperToDate
        OfferedWorkDayDTO offeredWorkDayDTO = offeredWorkDayMapper.toDto(offeredWorkDay);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHelperToDateMockMvc.perform(put("/api/helper-to-dates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offeredWorkDayDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HelperToDate in the database
        List<OfferedWorkDay> offeredWorkDayList = offeredWorkDayRepository.findAll();
        assertThat(offeredWorkDayList).hasSize(databaseSizeBeforeUpdate);
    }

//    @Test
//    @Transactional
    public void deleteHelperToDate() throws Exception {
        // Initialize the database
        offeredWorkDayRepository.saveAndFlush(offeredWorkDay);

        int databaseSizeBeforeDelete = offeredWorkDayRepository.findAll().size();

        // Delete the helperToDate
        restHelperToDateMockMvc.perform(delete("/api/helper-to-dates/{id}", offeredWorkDay.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OfferedWorkDay> offeredWorkDayList = offeredWorkDayRepository.findAll();
        assertThat(offeredWorkDayList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
