package ernteerfolg.backend.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import ernteerfolg.backend.ErnteErfolgBackendApp;
import ernteerfolg.backend.domain.Helper;
import ernteerfolg.backend.repository.HelperRepository;
import ernteerfolg.backend.service.dto.HelperDTO;
import ernteerfolg.backend.web.rest.TestUtil;
/**
 * Integration tests for the {@link UserExtendResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgBackendApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class HelperResourceIT {

	private static final String FIRSTNAME = "Max";
	private static final String LASTNAME = "Mustermann";

	@Autowired
    private HelperRepository helperRepository;

    @Autowired
    private MockMvc restUserExtendMockMvc;

    @Test
    @Transactional
    public void createHelper() throws Exception {
        int databaseSizeBeforeCreate = helperRepository.findAll().size();

        HelperDTO helperDTO = new HelperDTO();
        helperDTO.setFirstName(FIRSTNAME);
        helperDTO.setLastName(LASTNAME);
        helperDTO.setStreet("Musterstraße");
     	helperDTO.setHouseNr("3");
     	helperDTO.setZipCode("10101");
     	helperDTO.setCity("Musterort");
        
        restUserExtendMockMvc.perform(post("/api/helpers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(helperDTO)))
            .andExpect(status().isCreated());

        List<Helper> helperList = helperRepository.findAll();
        assertThat(helperList).hasSize(databaseSizeBeforeCreate + 1);
        Helper testHeper = helperList.get(helperList.size() - 1);
        assertThat(testHeper.getUserExtend().getUser().getFirstName()).isEqualTo(FIRSTNAME);
        assertThat(testHeper.getUserExtend().getUser().getLastName()).isEqualTo(LASTNAME);
        // Todo test other fields!
    }
}
