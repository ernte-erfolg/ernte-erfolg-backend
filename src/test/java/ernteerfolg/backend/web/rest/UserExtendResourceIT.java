package ernteerfolg.backend.web.rest;

import ernteerfolg.backend.ErnteErfolgBackendApp;
import ernteerfolg.backend.domain.UserExtend;
import ernteerfolg.backend.repository.UserExtendRepository;
import ernteerfolg.backend.service.UserExtendService;
import ernteerfolg.backend.service.dto.UserExtendDTO;
import ernteerfolg.backend.service.mapper.UserExtendMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserExtendResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgBackendApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class UserExtendResourceIT {

    private static final String DEFAULT_MOBILE_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    @Autowired
    private UserExtendRepository userExtendRepository;

    @Autowired
    private UserExtendMapper userExtendMapper;

    @Autowired
    private UserExtendService userExtendService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserExtendMockMvc;

    private UserExtend userExtend;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserExtend createEntity(EntityManager em) {
        UserExtend userExtend = new UserExtend()
            .mobilePhone(DEFAULT_MOBILE_PHONE)
            .phone(DEFAULT_PHONE);
        return userExtend;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserExtend createUpdatedEntity(EntityManager em) {
        UserExtend userExtend = new UserExtend()
            .mobilePhone(UPDATED_MOBILE_PHONE)
            .phone(UPDATED_PHONE);
        return userExtend;
    }

    @BeforeEach
    public void initTest() {
        userExtend = createEntity(em);
    }

//    @Test
//    @Transactional
    public void createUserExtend() throws Exception {
        int databaseSizeBeforeCreate = userExtendRepository.findAll().size();

        // Create the UserExtend
        UserExtendDTO userExtendDTO = userExtendMapper.toDto(userExtend);
        restUserExtendMockMvc.perform(post("/api/user-extends")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userExtendDTO)))
            .andExpect(status().isCreated());

        // Validate the UserExtend in the database
        List<UserExtend> userExtendList = userExtendRepository.findAll();
        assertThat(userExtendList).hasSize(databaseSizeBeforeCreate + 1);
        UserExtend testUserExtend = userExtendList.get(userExtendList.size() - 1);
        assertThat(testUserExtend.getMobilePhone()).isEqualTo(DEFAULT_MOBILE_PHONE);
        assertThat(testUserExtend.getPhone()).isEqualTo(DEFAULT_PHONE);
    }

//    @Test
//    @Transactional
    public void createUserExtendWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userExtendRepository.findAll().size();

        // Create the UserExtend with an existing ID
        userExtend.setId(1L);
        UserExtendDTO userExtendDTO = userExtendMapper.toDto(userExtend);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserExtendMockMvc.perform(post("/api/user-extends")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userExtendDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserExtend in the database
        List<UserExtend> userExtendList = userExtendRepository.findAll();
        assertThat(userExtendList).hasSize(databaseSizeBeforeCreate);
    }


//    @Test
//    @Transactional
    public void getAllUserExtends() throws Exception {
        // Initialize the database
        userExtendRepository.saveAndFlush(userExtend);

        // Get all the userExtendList
        restUserExtendMockMvc.perform(get("/api/user-extends?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userExtend.getId().intValue())))
            .andExpect(jsonPath("$.[*].mobilePhone").value(hasItem(DEFAULT_MOBILE_PHONE)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)));
    }
    
//    @Test
//    @Transactional
    public void getUserExtend() throws Exception {
        // Initialize the database
        userExtendRepository.saveAndFlush(userExtend);

        // Get the userExtend
        restUserExtendMockMvc.perform(get("/api/user-extends/{id}", userExtend.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userExtend.getId().intValue()))
            .andExpect(jsonPath("$.mobilePhone").value(DEFAULT_MOBILE_PHONE))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE));
    }

//    @Test
//    @Transactional
    public void getNonExistingUserExtend() throws Exception {
        // Get the userExtend
        restUserExtendMockMvc.perform(get("/api/user-extends/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

//    @Test
//    @Transactional
    public void updateUserExtend() throws Exception {
        // Initialize the database
        userExtendRepository.saveAndFlush(userExtend);

        int databaseSizeBeforeUpdate = userExtendRepository.findAll().size();

        // Update the userExtend
        UserExtend updatedUserExtend = userExtendRepository.findById(userExtend.getId()).get();
        // Disconnect from session so that the updates on updatedUserExtend are not directly saved in db
        em.detach(updatedUserExtend);
        updatedUserExtend
            .mobilePhone(UPDATED_MOBILE_PHONE)
            .phone(UPDATED_PHONE);
        UserExtendDTO userExtendDTO = userExtendMapper.toDto(updatedUserExtend);

        restUserExtendMockMvc.perform(put("/api/user-extends")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userExtendDTO)))
            .andExpect(status().isOk());

        // Validate the UserExtend in the database
        List<UserExtend> userExtendList = userExtendRepository.findAll();
        assertThat(userExtendList).hasSize(databaseSizeBeforeUpdate);
        UserExtend testUserExtend = userExtendList.get(userExtendList.size() - 1);
        assertThat(testUserExtend.getMobilePhone()).isEqualTo(UPDATED_MOBILE_PHONE);
        assertThat(testUserExtend.getPhone()).isEqualTo(UPDATED_PHONE);
    }

//    @Test
//    @Transactional
    public void updateNonExistingUserExtend() throws Exception {
        int databaseSizeBeforeUpdate = userExtendRepository.findAll().size();

        // Create the UserExtend
        UserExtendDTO userExtendDTO = userExtendMapper.toDto(userExtend);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserExtendMockMvc.perform(put("/api/user-extends")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userExtendDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserExtend in the database
        List<UserExtend> userExtendList = userExtendRepository.findAll();
        assertThat(userExtendList).hasSize(databaseSizeBeforeUpdate);
    }

//    @Test
//    @Transactional
    public void deleteUserExtend() throws Exception {
        // Initialize the database
        userExtendRepository.saveAndFlush(userExtend);

        int databaseSizeBeforeDelete = userExtendRepository.findAll().size();

        // Delete the userExtend
        restUserExtendMockMvc.perform(delete("/api/user-extends/{id}", userExtend.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserExtend> userExtendList = userExtendRepository.findAll();
        assertThat(userExtendList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
