package ernteerfolg.backend.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import ernteerfolg.backend.ErnteErfolgBackendApp;
import ernteerfolg.backend.domain.Farm;
import ernteerfolg.backend.domain.Task;
import ernteerfolg.backend.domain.Workday;
import ernteerfolg.backend.repository.FarmRepository;
import ernteerfolg.backend.repository.TaskRepository;
import ernteerfolg.backend.service.AddressService;
import ernteerfolg.backend.service.dto.AddressDTO;
import ernteerfolg.backend.web.rest.task.CreateTaskPayload;
import ernteerfolg.backend.web.rest.task.TaskResource;
import ernteerfolg.backend.web.rest.task.UpdateTaskPayload;
import ernteerfolg.backend.web.rest.task.WorkdayPayload;

/**
 * Integration tests for the {@link TaskResource} REST controller.
 */
@SpringBootTest(classes = ErnteErfolgBackendApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class TaskResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NR = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NR = "BBBBBBBBBB";

    private static final String DEFAULT_E_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_E_MAIL = "BBBBBBBBBB";

    private static final Long DEFAULT_NUMBER_OF_HELPERS = 1L;
    private static final Long UPDATED_NUMBER_OF_HELPERS = 2L;

    private static final String DEFAULT_DESCRIPTION_PLACE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_PLACE = "BBBBBBBBBB";

    private static final String DEFAULT_STREET = "AAAAAAAAAA";
    private static final String DEFAULT_HOUSE_NUMBER = "0815";
    private static final String DEFAULT_ZIP = "81829";
    private static final String DEFAULT_CITY = "München";
    
    private static final BigDecimal DEFAULT_LON = new BigDecimal(1);
    private static final BigDecimal UPDATED_LON = new BigDecimal(2);

    private static final BigDecimal DEFAULT_LAT = new BigDecimal(1);
    private static final BigDecimal UPDATED_LAT = new BigDecimal(2);

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Boolean DEFAULT_ARCHIVED = false;
    private static final Boolean UPDATED_ARCHIVED = true;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private FarmRepository farmRepository;
    
    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTaskMockMvc;

    private Task task;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Task createEntity(EntityManager em) {
        Task task = new Task()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .phoneNr(DEFAULT_PHONE_NR)
            .eMail(DEFAULT_E_MAIL)
            .numberOfHelpers(DEFAULT_NUMBER_OF_HELPERS)
            .descriptionPlace(DEFAULT_DESCRIPTION_PLACE)
            .lon(DEFAULT_LON)
            .lat(DEFAULT_LAT)
            .active(DEFAULT_ACTIVE)
            .archived(DEFAULT_ARCHIVED);
        return task;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Task createUpdatedEntity(EntityManager em) {
        Task task = new Task()
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .phoneNr(UPDATED_PHONE_NR)
            .eMail(UPDATED_E_MAIL)
            .numberOfHelpers(UPDATED_NUMBER_OF_HELPERS)
            .descriptionPlace(UPDATED_DESCRIPTION_PLACE)
            .lon(UPDATED_LON)
            .lat(UPDATED_LAT)
            .active(UPDATED_ACTIVE)
            .archived(UPDATED_ARCHIVED);
        return task;
    }

    @BeforeEach
    public void initTest() {
        task = createEntity(em);
    }

    @Test
    @Transactional
    public void searchTasks() throws Exception {
    
    	Task task = new Task(); // messetsadt
    	task.setTitle("Messestadt");
    	task.setLat(new BigDecimal("48.13138212235482"));
    	task.setLon(new BigDecimal("11.687564849853516"));
    	taskRepository.save(task);
	
	    // Feldkirchen
    	task = new Task();
    	task.setTitle("Feldkirchen");
    	task.setLat(new BigDecimal("48.14535788899587"));
    	task.setLon(new BigDecimal("11.734085083007814"));
    	taskRepository.save(task);

	    // Markt Schwaben
    	task = new Task();
    	task.setTitle("Markt Schwaben");
    	task.setLat(new BigDecimal("48.18577453931438"));
    	task.setLon(new BigDecimal("11.85527801513672"));
    	taskRepository.save(task);

        restTaskMockMvc.perform(get("/api/searchTasks?zip=81829"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].taskList.[*].title").value(hasItem("Messestadt")))
            .andExpect(jsonPath("$.[0].taskList.[*].title").value(hasItem("Feldkirchen")))
            .andExpect(jsonPath("$.[0].taskList.[*].title").value(hasItem("Markt Schwaben")));
    }
    	
    @Test
    @Transactional
    public void createTask() throws Exception {
        
    	Farm farm = new Farm();
    	farm.setName("Test");;
    	farm = farmRepository.save(farm);
    	
        int databaseSizeBeforeCreate = taskRepository.findAll().size();

        CreateTaskPayload  createPayload = new CreateTaskPayload();
        createPayload.setTitle(task.getTitle());
        createPayload.setDescription(task.getDescription());
        createPayload.setFarmId(farm.getId());
        createPayload.setStreet(DEFAULT_STREET);
        createPayload.setHouseNr(DEFAULT_HOUSE_NUMBER);
        createPayload.setZip(DEFAULT_ZIP);
        createPayload.setCity(DEFAULT_CITY);
        createPayload.setAdditionalInformation(task.getDescriptionPlace());
       
        Set<WorkdayPayload> requestedWorkdayDTOSet = new HashSet<>();
        requestedWorkdayDTOSet.add(new WorkdayPayload(LocalDate.of(2020, 2, 1), 5));
        requestedWorkdayDTOSet.add(new WorkdayPayload(LocalDate.of(2020, 2, 2), 3));
        
        createPayload.setRequestedDays(requestedWorkdayDTOSet);
        
		restTaskMockMvc.perform(post("/api/tasks")
		    .contentType(MediaType.APPLICATION_JSON)
		    .content(TestUtil.convertObjectToJsonBytes(createPayload)))
		    .andExpect(status().isCreated());

        // Validate the Task in the database
        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(databaseSizeBeforeCreate + 1);
        Task testTask = taskList.get(taskList.size() - 1);
        assertThat(testTask.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testTask.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTask.getDescriptionPlace()).isEqualTo(DEFAULT_DESCRIPTION_PLACE);
        assertThat(testTask.getLon()).isEqualTo("11.688307151494353");
        assertThat(testTask.getLat()).isEqualTo("48.13380130267043");
        
        Optional<AddressDTO> addressDTOOptional = addressService.findOne(testTask.getAddress().getId());
        assertThat(addressDTOOptional.isPresent());
        AddressDTO addressDTO = addressDTOOptional.get();
        
        assertThat(addressDTO.getStreet()).isEqualTo(DEFAULT_STREET);
        assertThat(addressDTO.getHouseNr()).isEqualTo(DEFAULT_HOUSE_NUMBER);
        assertThat(addressDTO.getZip()).isEqualTo(DEFAULT_ZIP);
        assertThat(addressDTO.getCity()).isEqualTo(DEFAULT_CITY);
        
        Set<Workday> helpers = testTask.getHelpers();
        assertThat(helpers.size()).isEqualTo(2);
        List<Workday> helperList = new ArrayList<Workday>(helpers);
		assertThat(helperList.get(0).getDate().isEqual(LocalDate.of(2020,2,1)));
		assertThat(helperList.get(0).getDurationPerDay()).isEqualTo(5);
		assertThat(helperList.get(1).getDate().isEqual(LocalDate.of(2020,2,2)));
		assertThat(helperList.get(1).getDurationPerDay()).isEqualTo(3);
    }

    private Task createTaskViaHTTP() throws Exception {
        
    	Farm farm = new Farm();
    	farm.setName("Test");;
    	farm = farmRepository.save(farm);
    	
        CreateTaskPayload  createPayload = new CreateTaskPayload();
        createPayload.setTitle(task.getTitle());
        createPayload.setDescription(task.getDescription());
        createPayload.setFarmId(farm.getId());
        createPayload.setStreet(DEFAULT_STREET);
        createPayload.setHouseNr(DEFAULT_HOUSE_NUMBER);
        createPayload.setZip(DEFAULT_ZIP);
        createPayload.setCity(DEFAULT_CITY);
        createPayload.setAdditionalInformation(task.getDescriptionPlace());
       
        Set<WorkdayPayload> requestedWorkdayDTOSet = new HashSet<>();
        requestedWorkdayDTOSet.add(new WorkdayPayload(LocalDate.of(2020, 2, 1), 5));
        requestedWorkdayDTOSet.add(new WorkdayPayload(LocalDate.of(2020, 2, 2), 3));
        
        createPayload.setRequestedDays(requestedWorkdayDTOSet);
        
		restTaskMockMvc.perform(post("/api/tasks")
		    .contentType(MediaType.APPLICATION_JSON)
		    .content(TestUtil.convertObjectToJsonBytes(createPayload)))
		    .andExpect(status().isCreated());

        // Validate the Task in the database
        List<Task> taskList = taskRepository.findAll();
        Task testTask = taskList.get(taskList.size() - 1);
		return testTask;
    }
    
    @Test
    @Transactional
    public void getAllTasksByFarm() throws Exception {

    	Farm farm = new Farm();
    	farm.setName("Test");
    	farm = farmRepository.save(farm);
    	task.setFarm(farm);
    	
    	// Initialize the database
        task = taskRepository.saveAndFlush(task);
        
        Task task2 = new Task()
                .title("task2")
                .description(DEFAULT_DESCRIPTION)
                .phoneNr(DEFAULT_PHONE_NR)
                .eMail(DEFAULT_E_MAIL)
                .numberOfHelpers(DEFAULT_NUMBER_OF_HELPERS)
                .descriptionPlace(DEFAULT_DESCRIPTION_PLACE)
                .lon(DEFAULT_LON)
                .lat(DEFAULT_LAT)
                .active(DEFAULT_ACTIVE)
                .archived(DEFAULT_ARCHIVED)
                .farm(farm);

        task2 = taskRepository.saveAndFlush(task2);

        Task task3 = new Task()
                .title("task3")
                .description(DEFAULT_DESCRIPTION)
                .phoneNr(DEFAULT_PHONE_NR)
                .eMail(DEFAULT_E_MAIL)
                .numberOfHelpers(DEFAULT_NUMBER_OF_HELPERS)
                .descriptionPlace(DEFAULT_DESCRIPTION_PLACE)
                .lon(DEFAULT_LON)
                .lat(DEFAULT_LAT)
                .active(DEFAULT_ACTIVE)
                .archived(DEFAULT_ARCHIVED)
                .farm(farm);
    	Farm farm2 = new Farm();
    	farm2.setName("Farm2");
    	farm2 = farmRepository.save(farm2);
    	task3.setFarm(farm2);

        task3 = taskRepository.saveAndFlush(task3);

        // Get all the taskList
        restTaskMockMvc.perform(get("/api/tasksByFarm?farmId={id}", farm.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.length()").value(2))
            .andExpect(jsonPath("$.[*].id").value(hasItem(task.getId().intValue())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(task2.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].title").value(hasItem("task2")))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.intValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.intValue())));
    }
    
    @Test
    @Transactional
    public void getTask() throws Exception {

    	Task task = createTaskViaHTTP();
    	
        // Get the task
        restTaskMockMvc.perform(get("/api/tasks/{id}", task.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(task.getId().intValue()))
            .andExpect(jsonPath("$.farmId").value(task.getFarm().getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.additionalInformation").value(DEFAULT_DESCRIPTION_PLACE))
            .andExpect(jsonPath("$.lon").value(task.getLon()))
            .andExpect(jsonPath("$.lat").value(task.getLat()))
        	.andExpect(jsonPath("$.street").value(DEFAULT_STREET))
        	.andExpect(jsonPath("$.houseNr").value(DEFAULT_HOUSE_NUMBER))
        	.andExpect(jsonPath("$.zip").value(DEFAULT_ZIP))
        	.andExpect(jsonPath("$.city").value(DEFAULT_CITY))
        	.andExpect(jsonPath("$.requestedDays.[0].date").value("2020-02-01"))
        	.andExpect(jsonPath("$.requestedDays.[0].workingHours").value("5"))
        	.andExpect(jsonPath("$.requestedDays.[1].date").value("2020-02-02"))
        	.andExpect(jsonPath("$.requestedDays.[1].workingHours").value("3"))
        	.andExpect(jsonPath("$.guaranteedDays.[0].date").value("2020-02-02"))
        	.andExpect(jsonPath("$.guaranteedDays.[0].workingHours").value("0"))
        	.andExpect(jsonPath("$.guaranteedDays.[1].date").value("2020-02-01"))
        	.andExpect(jsonPath("$.guaranteedDays.[1].workingHours").value("0"));
        
        // TODO check "helpers" & "guaranteedDaysByHelper"        
    }

    @Test
    @Transactional
    public void getNonExistingTask() throws Exception {
        // Get the task
        restTaskMockMvc.perform(get("/api/tasks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTask() throws Exception {

    	Farm farm = new Farm();
    	farm.setName("Test");
    	farm = farmRepository.save(farm);
    	task.setFarm(farm);
    	
    	// Initialize the database
        taskRepository.saveAndFlush(task);

        int databaseSizeBeforeUpdate = taskRepository.findAll().size();

        // Update the task
        Task taskToUpdate = taskRepository.findById(task.getId()).get();
        // Disconnect from session so that the updates on updatedTask are not directly saved in db
        em.detach(taskToUpdate);
        UpdateTaskPayload updateTaskPayload = new UpdateTaskPayload();
        updateTaskPayload.setId(taskToUpdate.getId());
        updateTaskPayload.setFarmId(taskToUpdate.getFarm().getId()); // Farm darf nie null sein!
        updateTaskPayload.setTitle(UPDATED_TITLE);
        updateTaskPayload.setDescription(UPDATED_DESCRIPTION);
        updateTaskPayload.setAdditionalInformation(UPDATED_DESCRIPTION_PLACE);
        
        restTaskMockMvc.perform(put("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updateTaskPayload)))
            .andExpect(status().isOk());

        // Validate the Task in the database
        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(databaseSizeBeforeUpdate);
        Task testTask = taskList.get(taskList.size() - 1);
        assertThat(testTask.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testTask.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTask.getDescriptionPlace()).isEqualTo(UPDATED_DESCRIPTION_PLACE);
    }

    @Test
    @Transactional
    public void updateNonExistingTask() throws Exception {
        int databaseSizeBeforeUpdate = taskRepository.findAll().size();

        UpdateTaskPayload updateTaskPayload = new UpdateTaskPayload();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTaskMockMvc.perform(put("/api/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updateTaskPayload)))
            .andExpect(status().isBadRequest());

        // Validate the Task in the database
        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(databaseSizeBeforeUpdate);
    }
}

