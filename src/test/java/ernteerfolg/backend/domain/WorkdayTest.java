package ernteerfolg.backend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

import java.time.LocalDate;

public class WorkdayTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Workday.class);
        Workday workday1 = new Workday();
        workday1.setId(1L);
        workday1.setDate(LocalDate.now());
        Workday workday2 = new Workday();
        workday2.setId(workday1.getId());
        workday2.setDate(workday1.getDate());
        assertThat(workday1).isEqualTo(workday2);
        workday2.setId(2L);
        workday2.setDate(LocalDate.MIN);
        assertThat(workday1).isNotEqualTo(workday2);
        workday1.setId(null);
        assertThat(workday1).isNotEqualTo(workday2);
    }
}
