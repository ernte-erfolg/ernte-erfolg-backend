package ernteerfolg.backend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

public class FarmTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FarmType.class);
        FarmType farmType1 = new FarmType();
        farmType1.setId(1L);
        FarmType farmType2 = new FarmType();
        farmType2.setId(farmType1.getId());
        assertThat(farmType1).isEqualTo(farmType2);
        farmType2.setId(2L);
        assertThat(farmType1).isNotEqualTo(farmType2);
        farmType1.setId(null);
        assertThat(farmType1).isNotEqualTo(farmType2);
    }
}
