package ernteerfolg.backend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

public class OfferedWorkDayTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OfferedWorkDay.class);
        OfferedWorkDay offeredWorkDay1 = new OfferedWorkDay();
        offeredWorkDay1.setId(1L);
        OfferedWorkDay offeredWorkDay2 = new OfferedWorkDay();
        offeredWorkDay2.setId(offeredWorkDay1.getId());
        assertThat(offeredWorkDay1).isEqualTo(offeredWorkDay2);
        offeredWorkDay2.setId(2L);
        assertThat(offeredWorkDay1).isNotEqualTo(offeredWorkDay2);
        offeredWorkDay1.setId(null);
        assertThat(offeredWorkDay1).isNotEqualTo(offeredWorkDay2);
    }
}
