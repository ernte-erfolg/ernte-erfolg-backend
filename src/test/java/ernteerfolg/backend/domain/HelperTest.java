package ernteerfolg.backend.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ernteerfolg.backend.web.rest.TestUtil;

public class HelperTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Helper.class);
        Helper helper1 = new Helper();
        helper1.setId(1L);
        Helper helper2 = new Helper();
        helper2.setId(helper1.getId());
        assertThat(helper1).isEqualTo(helper2);
        helper2.setId(2L);
        assertThat(helper1).isNotEqualTo(helper2);
        helper1.setId(null);
        assertThat(helper1).isNotEqualTo(helper2);
    }
}
